/bin/bash
set -euo pipefail
. $(dirname $0)/constants.sh
#Copy source to our depoyment group folder.
cp -rp $code_deploy_path $source_path/$DEPLOYMENT_GROUP_NAME
#Copy all the gitignored files(This is basically conf and related files)
cd $source_path/$DEPLOYMENT_GROUP_NAME
#get required modules for the app.
sudo yarn
node generate.config.script.js $application_name $DEPLOYMENT_GROUP_NAME
sudo yarn build
cd server 
sudo forever-service install $application_name-$DEPLOYMENT_GROUP_NAME --script server.js