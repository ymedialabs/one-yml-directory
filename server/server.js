require('dotenv').config();
let express = require('express');
let mongoose = require('mongoose');
let cors = require('cors');
let bodyParser = require('body-parser');
const passport = require('passport');
const http = require('http');
let database = require('./database/db');
const app = express();
const socketIO = require('socket.io');
const server = http.createServer(app);
const io = socketIO(server);
const path = require('path');
const apiRouter = require('./router');

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());
app.disable('etag');

mongoose.Promise = global.Promise;
mongoose
  .connect(database.db, {
    useNewUrlParser: true,
  })
  .then(
    () => {
      console.log('Database connected sucessfully !');
    },
    (error) => {
      console.log('Database could not be connected : ' + error);
    }
  );

app.use(passport.initialize());
require('./passport')(passport);
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());
app.use('/api', apiRouter);
app.use('/', express.static(path.resolve('../build')));
app.use('*', function (req, res) {
  res.setHeader('Cache-Control', 'public, max-age=0');
  res.sendFile(path.resolve('../build/index.html'));
});

const socketOps = require('./socketOps');
socketOps.allSocketOps(io);

const port = process.env.PORT;
server.listen(port, () => console.log(`Listening on port ${port}`));
