var request = require('request');
let database = require('./database/db');
let mongoose = require('mongoose');

mongoose.connect(
  database.db,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err, db) {
    if (err) throw err;
    db.collection('users').update(
      { employeeId: '' },
      { $push: { roles: 'superadmin' } },
      function (err, res) {
        if (err) throw err;
        console.log('document updated');
        db.close();
      }
    );
  }
);
