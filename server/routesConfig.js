const ROUTES = {
  LANDING: '/landing',
  SIGN_IN: '/signin',
  HOME: '/home',
  PROFILE: '/profile',
  EDIT_PROFILE: '/edit-profile/:id',
  ANY: '*',
  PAGE_NOT_FOUND: '/page-not-found',
  USER_DETAILS: '/user/:id',
  DASHBOARD_ROUTE: '/dashboard',
  VIDEO_PAGE: '/video-call',
};

const components = {
  HomePage: {
    component: 'HomePage',
    url: ROUTES.HOME,
    title: 'HomePage',
  },
  Profile: {
    component: 'Profile',
    url: ROUTES.PROFILE,
    title: 'Profile',
  },
  UserDetails: {
    component: 'UserDetails',
    url: ROUTES.USER_DETAILS,
    title: 'UserDetails',
  },
  EditProfilePage: {
    component: 'EditProfilePage',
    url: ROUTES.EDIT_PROFILE,
    title: 'EditProfilePage',
  },
  Dashboard: {
    component: 'Dashboard',
    url: ROUTES.DASHBOARD_ROUTE,
    title: 'Dashboard',
  },
  VideoPage: {
    component: 'VideoPage',
    url: ROUTES.VIDEO_PAGE,
    title: 'VideoPage',
  },
  PageNotFound: {
    component: 'PageNotFound',
    url: ROUTES.PAGE_NOT_FOUND,
    title: 'PageNotFound',
  },
};

const routesConfig = {
  superadmin: { routes: [...Object.values(components)] },
  user: { routes: [...Object.values(components)] },
};

module.exports = routesConfig;
