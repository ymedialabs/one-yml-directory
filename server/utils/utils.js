const routesConfig = require('../routesConfig');

const checkCaseInsensitive = (text) => {
  return new RegExp(text, 'gi');
};

const isDataExists = (arr, lengthToCheck = 0) =>
  arr && arr.length > lengthToCheck;

const checkSuperadmin = (roles) => roles && roles.includes('superadmin');

const getRouteDetails = (roles) =>
  checkSuperadmin(roles)
    ? routesConfig.superadmin.routes
    : routesConfig.user.routes;

const checkIfValidMailId = (mailId, pattern = `^[\\w.+\-]+@ymedialabs\.com$`) =>
  mailId.match(pattern);

module.exports = {
  checkCaseInsensitive,
  isDataExists,
  checkSuperadmin,
  getRouteDetails,
  checkIfValidMailId,
};
