let Post = require('../server/models/user-schema');

let checkUserSession = (req, res, next) => {
  Post.findById(req.body.loggedUserId, (error, data) => {
    if (error) {
      return next(error);
    } else {
      if (
        req.params.id === req.body.loggedUserId ||
        data.roles.includes('superadmin')
      ) {
        next();
      } else {
        return res.status(403).send({
          success: false,
          message: 'not authorized to perform this action',
        });
      }
    }
  });
};

module.exports = {
  checkUserSession: checkUserSession,
};
