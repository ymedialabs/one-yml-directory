let mongoose = require("mongoose"),
  express = require("express");

require("dotenv").config();
let secret = require("./config");

const Cryptr = require("cryptr");
const cryptr = new Cryptr(secret.SECRET_KEY);

let chats = require("./models/chat-schema");
(user = require("./models/user-schema")),
  (groups = require("./models/group-schema")),
  (groupChats = require("./models/group-chat-schema"));

const allSocketOps = io => {
  const userData = {},
    privateList = [],
    userList = [],
    rooms = [],
    userDetails = {};

  io.on("connection", socket => {
    socket.on("addUser", (name, userInfo) => {
      socket.username = name;
      userData[name] = socket.id;
      userList.push({
        username: name,
        socketID: socket.id,
        userID: userInfo._id
      });
      userDetails[name] = userInfo;
      io.emit("addUser", userData, userDetails);
    });

    socket.on("chat message", (msg, username, userId) => {
      let encodedData = cryptr.encrypt(msg);
      let chatRecord = new chats({
        senderId: userId,
        sender: username,
        message: encodedData,
        reciever: "all",
        recieverId: "all"
      });
      chatRecord.save(function(err, chatData) {
        if (err) return console.error(err);
      });

      io.emit("chat message", msg, username, userId, "all");
    });

    socket.on("private", (pvtMsg, sender, reciever) => {
      let sid = userList.filter(
        elem => elem.userID === sender._id || elem.userID === reciever._id
      );
      sid.forEach(elm =>
        io
          .to(`${elm.socketID}`)
          .emit(
            "private",
            pvtMsg,
            sender.name,
            reciever.name,
            sender._id,
            reciever._id
          )
      );
      let encodedData = cryptr.encrypt(pvtMsg);
      let privateRecord = new chats({
        senderId: sender._id,
        sender: sender.name,
        message: encodedData,
        reciever: reciever.name,
        recieverId: reciever._id
      });
      privateRecord.save(function(err, chatData) {
        if (err) return console.error(err);
      });
    });
    socket.on("addRoom", (groupId, roomName, memberList, createdBy) => {
      rooms.push(roomName);
      let groupData = new groups({
        groupId: groupId,
        groupname: roomName,
        members: memberList,
        createdBy: createdBy.name,
        creatorId: createdBy._id
      });
      groupData.save((err, data) => {
        if (err) return console.error(err);
      });
      io.emit("addRoom", {
        groupId: groupId,
        groupname: roomName,
        members: memberList,
        createdBy: createdBy.name,
        creatorId: createdBy._id
      });
    });

    socket.on("joinGroup", (roomId, room, memberList) => {
      groups.findOne({ groupId: roomId }, (err, data) => {
        if (err) {
          console.log(err);
        }
        memberList.forEach(member => {
          if (data.members.includes(member)) {
          } else {
            groups.updateOne(
              { groupname: room },
              { $push: { members: member } },
              (err, data) => {
                if (err) {
                  console.log(err);
                }
              }
            );
          }
        });
      });
    });

    socket.on("groupMessage", (groupData, msg, sender) => {
      let encodedData = cryptr.encrypt(msg);
      let groupMessageData = new groupChats({
        senderId: sender._id,
        sender: sender.name,
        message: encodedData,
        groupname: groupData.groupname,
        groupId: groupData.groupId
      });

      groups.findOne({ groupId: groupData.groupId }, (err, data) => {
        if (err) {
          console.log(err);
        }
        let groupMembers = data.members;

        if (groupMembers.includes(sender.name)) {
          let groupIds = userList.filter(user => {
            if (groupMembers.includes(user.username)) {
              return user;
            }
          });
          groupIds.forEach(member =>
            io
              .to(`${member.socketID}`)
              .emit(
                "groupMessage",
                msg,
                sender.name,
                data.groupname,
                data.groupId
              )
          );

          groupMessageData.save((err, data) => {
            if (err) {
              console.log(err);
            }
          });
        }
      });
    });
    socket.on("disconnect", () => {
      console.log("user disconnected");
    });
  });
};
module.exports = {
  allSocketOps
};
