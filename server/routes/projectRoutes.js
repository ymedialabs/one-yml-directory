const express = require('express');
const app = express();
const projectRoute = express.Router();
const middleware = require('../middleware');
const { isDataExists, checkCaseInsensitive } = require('../utils/utils');
let Project = require('../models/project-schema.js');

projectRoute
  .route('/project-search')
  .get(middleware.checkToken, function (req, res) {
    if (req.query.searchString) {
      const regex = checkCaseInsensitive(req.query.searchString);
      Project.find({ name: regex }, function (err, allProjects) {
        if (err) {
          res.status(400).send({ error: 'Invalid request' });
        } else {
          res.json(allProjects);
        }
      }).sort({ name: 1 });
    }
  });

projectRoute
  .route('/add')
  .post(middleware.checkToken, ({ body: { name, isComplete } = null }, res) => {
    const nameToInsert = checkCaseInsensitive(name);
    Project.find({ name: nameToInsert }, (err, projects) => {
      if (isDataExists(projects)) {
        res.status(400).send({ error: 'Already exists' });
      } else {
        Project.insertMany({ name, isComplete }, async (err, project) => {
          if (err) res.json(err);
          else {
            res.json({ text: 'Added successfully', project: project[0] });
          }
        });
      }
    });
  });

module.exports = projectRoute;
