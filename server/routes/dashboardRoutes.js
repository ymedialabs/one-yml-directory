let mongoose = require('mongoose'),
  express = require('express'),
  router = express.Router();
const middleware = require('../middleware'),
  jwt = require('jsonwebtoken');

require('dotenv').config();
let secret = require('../config');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(secret.SECRET_KEY);

let chats = require('../models/chat-schema');
(groups = require('../models/group-schema')),
  (groupChat = require('../models/group-chat-schema')),
  (user = require('../models/user-schema'));

router.route('/all').get(middleware.checkToken, (req, res) => {
  chats.find({ reciever: 'all' }, async (error, data) => {
    if (error) {
      return next(error);
    } else {
      data &&
        data.forEach((elem) => (elem.message = cryptr.decrypt(elem.message)));
      await res.json(data);
    }
  });
});

router.route('/private').get(middleware.checkToken, (req, res) => {
  chats.find(
    {
      $or: [
        {
          $and: [
            { senderId: req.query.senderId },
            { recieverId: req.query.recieverId },
          ],
        },
        {
          $and: [
            { recieverId: req.query.senderId },
            { senderId: req.query.recieverId },
          ],
        },
      ],
    },
    (err, data) => {
      if (err) {
        return next(err);
      } else {
        data.forEach((elem) => (elem.message = cryptr.decrypt(elem.message)));
        res.json(data);
      }
    }
  );
});

router.route('/grouplist').get(middleware.checkToken, (req, res) => {
  groups.find((error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

router.route('/currentgroup').get(middleware.checkToken, (req, res) => {
  groups.findOne({ groupId: req.query.groupId }, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

router.route('/groupmessage').get(middleware.checkToken, (req, res) => {
  groups.findOne({ groupId: req.query.groupId }, (err, data) => {
    if (err) throw err;
    try {
      if (data.members.length > 0 && data.members.includes(req.query.sender)) {
        groupChat.find({ groupId: req.query.groupId }, (err, data) => {
          if (err) {
            console.log(err);
          }
          data.forEach((elem) => (elem.message = cryptr.decrypt(elem.message)));
          res.json(data);
        });
      } else {
        res.json([]);
      }
    } catch (error) {
      error;
    }
  });
});

router.route('/userlist').get((req, res) => {
  user.find((error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

module.exports = router;
