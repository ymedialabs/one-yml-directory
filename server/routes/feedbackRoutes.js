const express = require('express');
const app = express();
const feedbackRoute = express.Router();
const middleware = require('../middleware');
let Feedback = require('../models/feedback-schema');

feedbackRoute.route('/list').get(middleware.checkToken, function (req, res) {
  Feedback.find({}, function (err, feedbacks) {
    if (err) {
      res.status(400).send({ error: 'Invalid request' });
    } else {
      res.json({ list: feedbacks });
    }
  });
});

feedbackRoute.route('/add').post(middleware.checkToken, ({ body }, res) => {
  Feedback.insertMany({ ...body }, async (err, page) => {
    if (err) res.json(err);
    else {
      res.json({ text: 'Added successfully' });
    }
  });
});

module.exports = feedbackRoute;
