const express = require('express');
const app = express();
const tutorialRoute = express.Router();
const middleware = require('../middleware');
let Tutorial = require('../models/tutorial-schema');
const { isDataExists, checkCaseInsensitive } = require('../utils/utils');

tutorialRoute.route('/all').get(middleware.checkToken, function (req, res) {
  Tutorial.find({}, function (err, allPages) {
    if (err) {
      res.status(400).send({ error: 'Invalid request' });
    } else {
      res.json({ pages: allPages });
    }
  });
});

tutorialRoute
  .route('/add')
  .post(middleware.checkToken, ({ body: { name } = null }, res) => {
    const nameToInsert = checkCaseInsensitive(name);
    Tutorial.find({ name: nameToInsert }, (err, pages) => {
      if (isDataExists(pages)) {
        res.status(400).send({ error: 'Already exists' });
      } else {
        Tutorial.insertMany({ name }, async (err, page) => {
          if (err) res.json(err);
          else {
            res.json({ text: 'Added successfully', page: page[0] });
          }
        });
      }
    });
  });

module.exports = tutorialRoute;
