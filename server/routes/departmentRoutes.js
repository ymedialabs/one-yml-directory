const express = require('express');
const app = express();
const departmentRoute = express.Router();
const middleware = require('../middleware');
let Department = require('../models/department-schema.js');
const { isDataExists, checkCaseInsensitive } = require('../utils/utils');

departmentRoute.route('/list').get(middleware.checkToken, (req, res) => {
  Department.find(
    {},
    null,
    { sort: { value: 1 }, collation: { locale: 'en' } },
    (err, allDepartments) => {
      if (err) {
        res.status(400).send({ error: 'Invalid request' });
      } else {
        res.json({ list: allDepartments });
      }
    }
  );
});

departmentRoute
  .route('/add')
  .post(middleware.checkToken, ({ body: { value, label } = null }, res) => {
    const nameToInsert = checkCaseInsensitive(value);
    Department.find({ value: nameToInsert }, (err, pages) => {
      if (isDataExists(pages)) {
        res.status(400).send({ error: 'Already exists' });
      } else {
        Department.insertMany({ value, label }, async (err, page) => {
          if (err) res.json(err);
          else {
            res.json({ text: 'Added successfully' });
          }
        });
      }
    });
  });

module.exports = departmentRoute;
