let database = require('./database/db');
let mongoose = require('mongoose');
mongoose.connect(database.db, function (err, db) {
  if (err) throw err;
  db.collection('projects').deleteMany({}, function (err, res) {
    if (err) throw err;
    db.close();
  });
});
