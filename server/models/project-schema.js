const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let projectSchema = new Schema(
  {
    name: {
      type: String,
    },
    isComplete: {
      type: Boolean,
    },
  },
  {
    collection: 'projects',
  }
);

module.exports = mongoose.model('projects', projectSchema);
