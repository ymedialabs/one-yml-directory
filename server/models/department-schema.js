const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let departmentSchema = new Schema(
  {
    value: {
      type: String,
    },
    label: {
      type: String,
    },
  },
  {
    collection: 'departments',
  }
);

module.exports = mongoose.model('departments', departmentSchema);
