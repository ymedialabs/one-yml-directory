const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let feedbackSchema = new Schema(
  {
    feedback: {
      type: String,
    },
    user: {
      type: Object,
    },
  },
  {
    collection: 'feedback',
  }
);

module.exports = mongoose.model('feedback', feedbackSchema);
