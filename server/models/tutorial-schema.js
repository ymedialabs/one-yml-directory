const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let tutorialSchema = new Schema(
  {
    name: {
      type: String,
    },
  },
  {
    collection: 'tutorials',
  }
);

module.exports = mongoose.model('tutorials', tutorialSchema);
