const express = require('express');
const router = express.Router();

const postroutes = require('./routes/PostRoute');
const dashboardRoute = require('./routes/dashboardRoutes');
const projectRoutes = require('./routes/projectRoutes');
const tutorialRoutes = require('./routes/tutorialRoutes');
const departmentRoutes = require('./routes/departmentRoutes');
const feedbackRoutes = require('./routes/feedbackRoutes');

router.use('/users', postroutes);
router.use('/dashboard', dashboardRoute);
router.use('/project', projectRoutes);
router.use('/tutorial', tutorialRoutes);
router.use('/department', departmentRoutes);
router.use('/feedback', feedbackRoutes);

module.exports = router;
