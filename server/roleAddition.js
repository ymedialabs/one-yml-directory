var request = require('request');
let database = require('./database/db');
let mongoose = require('mongoose');

mongoose.connect(
  database.db,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err, db) {
    if (err) throw err;

    db.collection('users').updateMany(
      {},
      { $set: { roles: ['user'] } },
      function (err, res) {
        if (err) throw err;
        console.log('documents updated');
        db.close();
      }
    );
  }
);
