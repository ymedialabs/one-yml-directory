/* eslint-disable no-console */
const { GetEnvVars } = require('env-cmd');
const SSM = require('aws-ssm-params');
const fs = require('fs');
const PARAMETRIC_STORE_CONSTANTS = require('./parametric-store.constants');

/**
 * @description
 * This are key which are stored in AWS parameter store
 *
 * This are the keys which we have to add in parameter store
 * port = `${projectName}/${instanceEnvironment}/port`;
 * baseUrl = `${projectName}/${instanceEnvironment}/baseUrl`;
 *
 * Value section add value for it.
 */
const generateFile = async () => {
  /**
   * @description
   * This variable of .env file path, config file path and config name.
   */
  const envFilePath = '.env.development.local';
  const envServerFilePath = 'server/.env';
  const envMainFilePath = '.env';

  /**
   * @description
   * This method is used to extract value from .env file where file path is mentioned in envFilePath variable. This method will check if environment variable from given path.
   * If it gets file then it will return env file value else it will return null
   *
   * @requires 'env-cmd' which is the library used to extract data from .env file
   *
   * @tutorial 'https://www.npmjs.com/package/env-cmd'
   *
   * @returns {object | null}
   * This method will return object with value extracted from .env file. If .env file is not there then it will return null
   */
  const envPromise = async () => {
    try {
      return await GetEnvVars({
        envFile: {
          filePath: envFilePath,
        },
      });
    } catch (error) {
      return null;
    }
  };

  // store environment value
  const environmentVariable = await envPromise();

  // SSM params data format and it's variable
  /**
   * @deprecated
   * instanceType is type of instance for which we want to connect to, for example dev, staging, pre-prod, prod etc
   *
   * @usage
   * @command node generate.config.js dev
   * @explanation when we execute command in our terminal, "dev" is considered as instance type.
   *
   * @example instanceType: dev, staging, pre-prod, prod
   */
  const instanceEnvironment = process.argv[3];

  /**
   * @description
   * Every project deployed in AWS will have it's own identity. Whose difference can be made via project name, this is used to managing IAM polices.
   */
  const projectName = process.argv[2];

  /**
   * @deprecated
   * For SSM params we need to know from which region our server are hosted. Based on which aws-ssm-params package will connect to that particular server and access data.
   */
  const region = 'us-west-2';
  const storePath = `/${projectName}/${instanceEnvironment}/`;

  /**
   * @description
   * SSM params object which is used by aws-ssm-params to connect to AWS instance. which will contain path and open to decrypt content.
   */
  const ssmParams = {
    Path: storePath,
    WithDecryption: true,
  };

  /**
   * @description
   * This object we will be adding access key, secret key and region value, to which this instance it will get connected to
   */
  let awsParams = { region };

  // since null is considered falsy in JavaScript, based on this we can access ACCESS_KEY and ACCESS_SECRET_KEY.
  if (environmentVariable) {
    // Value extracted from .env file
    const { ACCESS_KEY_ID, SECRET_ACCESS_KEY } = environmentVariable;
    awsParams = {
      accessKeyId: ACCESS_KEY_ID,
      secretAccessKey: SECRET_ACCESS_KEY,
      ...awsParams,
    };
  }

  /**
   * @description
   * This method is used to get data from SSM parameter store and create a file
   */
  let parameters;
  try {
    parameters = await SSM(ssmParams, awsParams);
  } catch (error) {
    return console.error(`error: ${error}`);
  }

  /**
   * @description
   * Creating env file which will be used to deploy web app to different backend endpoint. Since entire application will be extracting environment variables from .env file.
   *
   * @structure
   *VARIABLE_NAME_ONE=value
   *VARIABLE_NAME_TWO=value
   *
   * @result
   * This method will create a file .env (sample structure is given above)
   */
  const envFileContentSrc = `
  REACT_APP_BASE_URL=${
    parameters[`${storePath}${PARAMETRIC_STORE_CONSTANTS.REACT_APP_BASE_URL}`]
  }
  OAUTH_CLIENT_ID=${
    parameters[`${storePath}${PARAMETRIC_STORE_CONSTANTS.OAUTH_CLIENT_ID}`]
  }
  `;

  /**
   * @description
   * Creating env file which will be used to deploy web app to different backend endpoint. Since entire application will be extracting environment variables from .env file.
   *
   * @structure
   *VARIABLE_NAME_ONE=value
   *VARIABLE_NAME_TWO=value
   *
   * @result
   * This method will create a file .env (sample structure is given above)
   */
  const envFileContentServer = `
  REACT_APP_DB_URL=${
    parameters[`${storePath}${PARAMETRIC_STORE_CONSTANTS.REACT_APP_DB_URL}`]
  }
  SECRET_KEY=${
    parameters[`${storePath}${PARAMETRIC_STORE_CONSTANTS.SECRET_KEY}`]
  }
  PORT=${parameters[`${storePath}${PARAMETRIC_STORE_CONSTANTS.PORT}`]}
  `;

  // create .env file
  fs.writeFile(envMainFilePath, envFileContentSrc, (err) => {
    if (err) {
      return console.error('Could not create .env file');
    }
    return console.log(
      `Created .env file for ${instanceEnvironment} environment \n\n${envFileContentSrc}`
    );
  });

  // create .env file
  fs.writeFile(envServerFilePath, envFileContentServer, (err) => {
    if (err) {
      return console.error('Could not create .env file');
    }
    return console.log(
      `Created .env file for ${instanceEnvironment} environment \n\n${envFileContentServer}`
    );
  });
};

// Check if file has argument passed with it always
if (process.argv.length === 4) {
  generateFile();
} else {
  console.error(
    `Invalid usage of script.\n\nUsage: node generate.config.js ${process.argv[2]} ${process.argv[3]}`
  );
}
