import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import NumberFormat from 'react-number-format';

const NumberFormatContainer = ({
  customClass,
  name,
  label,
  format,
  mask,
  handleChange,
  handleKeyPress,
  placement,
  placeholder,
  thousandSeparator,
  type,
  isAllowed,
  handleBlur,
  handleFocus,
  onValueChange,
  onKeyUp,
  onKeyDown,
  inputVal,
  onMouseUp,
  ...props
}) => {
  const getClassNames = () => cx(['number-format-container', customClass]);
  const [inputValue, onInputChange] = useState('');
  useEffect(() => {
    const { length } = inputValue;
    if (length > 0 && inputVal !== inputValue) {
      handleChange(inputValue, name);
    } else {
      onInputChange(inputVal || '');
    }
  }, [inputValue, handleChange, inputVal, name]);
  return (
    <NumberFormat
      className={getClassNames()}
      value={inputValue}
      name={name}
      label={label}
      placement={placement}
      onChange={({ target: { value } }) => {
        handleChange(value, name);
      }}
      onKeyPress={handleKeyPress}
      format={format}
      mask={mask}
      placeholder={placeholder}
      thousandSeparator={thousandSeparator}
      type={type}
      isAllowed={() => true}
      onBlur={handleBlur}
      onFocus={handleFocus}
      onValueChange={onValueChange}
      onKeyDown={onKeyDown}
      onKeyUp={onKeyUp}
      onMouseUp={onMouseUp}
      defaultValue={inputVal}
      {...props}
    />
  );
};

NumberFormatContainer.propTypes = {
  customClass: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
  format: PropTypes.string,
  mask: PropTypes.string,
  handleChange: PropTypes.func,
  handleKeyPress: PropTypes.func,
  placement: PropTypes.string,
  placeholder: PropTypes.string,
  thousandSeparator: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  type: PropTypes.string,
  isAllowed: PropTypes.func,
  handleFocus: PropTypes.func,
  handleBlur: PropTypes.func,
  onValueChange: PropTypes.func,
  onKeyDown: PropTypes.func,
  onKeyUp: PropTypes.func,
  inputVal: PropTypes.string,
  onMouseUp: PropTypes.func,
};

NumberFormatContainer.defaultProps = {
  customClass: '',
  name: '',
  label: '',
  format: '(###) ###-####',
  mask: '_',
  handleChange: () => {},
  handleKeyPress: () => {},
  placement: 'bottom',
  placeholder: '',
  thousandSeparator: false,
  type: 'text',
  isAllowed: () => {},
  handleBlur: () => {},
  handleFocus: () => {},
  onValueChange: () => {},
  onKeyDown: () => {},
  onKeyUp: () => {},
  inputVal: '',
  onMouseUp: () => {},
};

export default React.memo(NumberFormatContainer);
