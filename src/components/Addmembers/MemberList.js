import React from 'react';
import PropTypes from 'prop-types';
import MemberItem from './MemberItem';
import { isDataExists } from '../../utils/utils';
import './member.scss';

const MemberList = ({ items, removeItem }) => (
  <div className="members-list">
    {isDataExists(items) &&
      items.map((item, index) => (
        <MemberItem
          key={Math.random().toString(36).substring(2, 15)}
          text={item}
          itemIndex={index}
          removeItem={removeItem}
        />
      ))}
  </div>
);

MemberList.propTypes = {
  items: PropTypes.array,
  removeItem: PropTypes.func,
};

MemberList.defaultProps = {
  items: [],
  removeItem: () => {},
};
export default MemberList;
