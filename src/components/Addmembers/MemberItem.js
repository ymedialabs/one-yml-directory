import React from 'react';
import PropTypes from 'prop-types';

const MemberItem = ({ text, itemIndex, removeItem }) => {
  const deleteItem = () => {
    removeItem(itemIndex);
  };
  return (
    <div className="member-card-container">
      <label className="form-check-label">{text}</label>
      <button type="button" className="form-check-button" onClick={deleteItem}>
        x
      </button>
    </div>
  );
};

MemberItem.propTypes = {
  text: PropTypes.string,
  itemIndex: PropTypes.number,
  removeItem: PropTypes.func,
};

MemberItem.defaultProps = {
  text: '',
  itemIndex: -1,
  removeItem: () => {},
};
export default MemberItem;
