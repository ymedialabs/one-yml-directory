import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../../store/actions';
import * as ROUTES from '../../constants/routes';
import UserIinitalComponent from '../UserIinitalComponent/UserIinitalComponent';
import ymlLogo from '../../assets/icn-logo3x.png';
import icnClose from '../../assets/close-icn.svg';
import HeaderSearchBar from '../HeaderSearchBar/HeaderSearchBar';
import ModalComponent from '../ModalComponent/ModalComponent';
import Button from '../Button/Button';
import './mobileHeader.scss';

const SEARCH_PLACEHOLDER_TEXT = 'Search';

class MobileHeader extends Component {
  state = {
    showLogout: false,
    showProfilePopUp: false,
  };

  onProfileClick = (route = null) => {
    this.toggleProfilePopup(false);
    const { history } = this.props;
    if (route) {
      history.push(route);
    }
  };

  renderImageContainer = () =>
    this.props.user.avatarUrl ? (
      <img
        className="profile-image"
        src={this.props.user.avatarUrl}
        alt="user-profile"
      />
    ) : (
      <UserIinitalComponent
        className="header-profile-user-image"
        name={this.props.user.name}
      />
    );

  getProfilePopUpList = () => [
    {
      text: 'Profile',
      onClickHandler: () => {
        this.onProfileClick(ROUTES.PROFILE);
      },
    },
    {
      text: 'Logout',
      onClickHandler: () => {
        this.toggleLogoutPopUp(true);
      },
    },
  ];

  toggleProfilePopup = (profilePopUpState) => {
    this.setState({ showProfilePopUp: profilePopUpState }, () => {
      const { showProfilePopUp } = this.state;
      if (showProfilePopUp) {
        document.querySelector('body').classList.add('overflow-hide');
      } else {
        document.querySelector('body').classList.remove('overflow-hide');
      }
    });
  };

  toggleLogoutPopUp = (popUpState) => {
    this.toggleProfilePopup(false);
    this.setState({ showLogout: popUpState });
  };

  renderProfileListItem = () =>
    [...this.getProfilePopUpList()].map((profileListItem, index) => (
      <li key={index} onClick={profileListItem.onClickHandler}>
        {profileListItem.text}
      </li>
    ));

  renderProfilePopUp = () => (
    <div className="profile-popup-container">
      <div className="popup-header-content">
        <img
          src={ymlLogo}
          alt="logo"
          className="yml-logo"
          onClick={() => {
            this.onProfileClick(ROUTES.HOME);
          }}
        />
        <img
          src={icnClose}
          alt="close"
          onClick={() => {
            this.toggleProfilePopup(false);
          }}
        />
      </div>
      <div className="list-container">
        <ul className="profile-list">{this.renderProfileListItem()}</ul>
      </div>
    </div>
  );

  logout = () => {
    this.toggleLogoutPopUp(false);
    this.props.logoutUser(this.props.history);
  };

  getBtnConfiguration = () => [
    {
      value: 'Cancel',
      handler: () => {
        this.toggleLogoutPopUp(false);
      },
      disabled: false,
      customClass: 'logout-modal-cancel',
    },
    {
      value: 'Log out',
      handler: this.logout,
      disabled: false,
      customClass: 'logout-btn',
    },
  ];

  renderActionContainer = () =>
    this.getBtnConfiguration().map((btn, index) => (
      <Button
        key={index}
        value={btn.value}
        onClick={btn.handler}
        disabled={btn.disabled}
        customClass={btn.customClass}
      />
    ));

  renderModalChildren = () => (
    <div className="logout-modal">
      <p>Do you want to log out?</p>
      <div className="action-container">{this.renderActionContainer()}</div>
    </div>
  );

  renderModal = () => (
    <ModalComponent>{this.renderModalChildren()}</ModalComponent>
  );

  render() {
    const { showProfilePopUp, showLogout } = this.state;
    return (
      <nav className="mobile-header-container">
        <div className="logo-search">
          <Link to={ROUTES.HOME}>
            <img src={ymlLogo} className="yml-logo" alt="YML-logo" />
          </Link>
          <HeaderSearchBar
            placeHolder={SEARCH_PLACEHOLDER_TEXT}
            customClass="mobile-search"
          />
        </div>
        <div
          role="button"
          onClick={() => {
            this.toggleProfilePopup(true);
          }}
        >
          {this.renderImageContainer()}
        </div>
        {showProfilePopUp && this.renderProfilePopUp()}
        {showLogout && this.renderModal()}
      </nav>
    );
  }
}

MobileHeader.propTypes = {
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
};
const mapDispatchToProps = (dispatch) => ({
  logoutUser: (history) => dispatch(logoutUser(history)),
});

export default withRouter(connect(null, mapDispatchToProps)(MobileHeader));
