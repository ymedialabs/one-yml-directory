import React from 'react';
import PropTypes from 'prop-types';
import SkillsList from './SkillsList';
import SkillsForm from './SkillsForm';
import './addSkills.scss';

const TAP_MESSAGE = 'Tap enter to add skill';
const TAP_MESSAGE_PROJECT = 'Tap enter to add project';
class AddSkills extends React.Component {
  state = {
    skills: this.props.skills,
  };

  addItem = (todoItem) => {
    this.props.skills.push(todoItem.newItemValue);
    this.props.handleAdminState();
    this.setState({ skills: this.props.skills }, this.props.enableBtn);
  };

  removeItem = (itemIndex) => {
    this.props.skills.splice(itemIndex, 1);
    this.props.handleAdminState();
    this.setState({ skills: this.props.skills }, this.props.enableBtn);
  };

  getTapMessage = () =>
    this.props.fromProject ? TAP_MESSAGE_PROJECT : TAP_MESSAGE;

  render() {
    const { skills } = this.state;
    const { showSkillsList, placeHolder } = this.props;
    return (
      <div id="main">
        {showSkillsList && (
          <SkillsList
            customClass={this.props.customClass}
            items={skills}
            removeItem={this.removeItem}
            skillDisplayStatus={this.props.skillDisplayStatus}
            showCross
          />
        )}

        <SkillsForm
          addItem={this.addItem}
          tapMessage={this.getTapMessage()}
          placeHolder={placeHolder}
        />
      </div>
    );
  }
}
AddSkills.propTypes = {
  customClass: PropTypes.string,
  skills: PropTypes.array,
  skillDisplayStatus: PropTypes.bool.isRequired,
  enableBtn: PropTypes.func,
  showSkillsList: PropTypes.bool,
  placeHolder: PropTypes.string,
  fromProject: PropTypes.bool,
  handleAdminState: PropTypes.func,
};

AddSkills.defaultProps = {
  customClass: '',
  skills: [],
  enableBtn: () => {},
  showSkillsList: true,
  placeHolder: 'Add Skill',
  fromProject: false,
  handleAdminState: () => {},
};
export default AddSkills;
