import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import PopOver from '../PopOver/PopOver';
import SkillsListItem from './SkillsListItem';
import { isDataExists } from '../../utils/utils';

const ITEMS_TO_SHOW = 4;

const SkillsList = ({
  items,
  removeItem,
  customClass,
  showCross,
  ...props
}) => {
  const [showPopUp, togglePopUp] = useState(false);

  const getSkillListClass = () => cx(['skills-list', customClass]);

  const renderSkills = (arraySent) =>
    [...arraySent].map((item, index) => (
      <SkillsListItem
        key={index}
        item={item}
        index={index}
        removeItem={removeItem}
        showCross={showCross}
      />
    ));

  const getRemainingLengthText = () => (
    <span className="additional-skills-text">
      {`+ ${items.length - ITEMS_TO_SHOW}`}
    </span>
  );

  const getPopUpChildrenArray = () => [
    ...items.slice(ITEMS_TO_SHOW, items.length),
  ];

  const renderPopUpChildren = () =>
    [...getPopUpChildrenArray()].map((item, index) => (
      <li key={index}>
        <span>{item}</span>
        {index !== [...getPopUpChildrenArray()].length - 1 && <span>,</span>}
      </li>
    ));

  const renderPopUp = () => (
    <PopOver
      customClass="skills-pop-over"
      handleOutsideClick={() => {
        togglePopUp(false);
      }}
    >
      <ul>{renderPopUpChildren()}</ul>
    </PopOver>
  );

  const renderProfileSkills = () => {
    const displaySkills = [...items];
    if (isDataExists(displaySkills, ITEMS_TO_SHOW)) {
      displaySkills.splice(ITEMS_TO_SHOW, items.length);
    }
    return (
      <>
        {renderSkills(displaySkills)}
        {isDataExists(items, ITEMS_TO_SHOW) && (
          <div
            onClick={() => {
              togglePopUp(!showPopUp);
            }}
            className="skills-list-additional"
            role="button"
          >
            {getRemainingLengthText()}
            {showPopUp && renderPopUp()}
          </div>
        )}
      </>
    );
  };

  return (
    <div className={getSkillListClass()}>
      {!props.skillDisplayStatus && isDataExists(items)
        ? renderSkills(items)
        : renderProfileSkills()}
    </div>
  );
};

SkillsList.propTypes = {
  skillDisplayStatus: PropTypes.bool.isRequired,
  items: PropTypes.array,
  removeItem: PropTypes.func,
  customClass: PropTypes.string,
  showCross: PropTypes.bool,
};

SkillsList.defaultProps = {
  items: [],
  removeItem: () => {},
  customClass: '',
  showCross: false,
};
export default SkillsList;
