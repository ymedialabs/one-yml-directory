import React from 'react';
import PropTypes from 'prop-types';

class SkillsListItem extends React.Component {
  onClickClose = () => {
    const index = Number(this.props.index);
    this.props.removeItem(index);
  };

  render() {
    return (
      <div className="skill-card-container">
        <span className="skill-item">{this.props.item}</span>
        {this.props.showCross && (
          <span
            role="button"
            className="form-check-botton"
            onClick={this.onClickClose}
          >
            &times;
          </span>
        )}
      </div>
    );
  }
}

SkillsListItem.propTypes = {
  item: PropTypes.string,
  removeItem: PropTypes.func.isRequired,
  index: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  showCross: PropTypes.bool,
};

SkillsListItem.defaultProps = {
  item: '',
  index: -1,
  showCross: false,
};
export default SkillsListItem;
