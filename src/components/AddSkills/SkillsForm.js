import React from 'react';
import PropTypes from 'prop-types';
import { isDataExists } from '../../utils/utils';

class SkillForm extends React.Component {
  constructor() {
    super();
    this.inputRef = React.createRef();
    this.state = {
      value: '',
    };
  }

  onSubmitForm = (event) => {
    event.preventDefault();
    const newItemValue = this.inputRef.current.value;
    if (newItemValue) {
      this.props.addItem({ newItemValue });
      this.resetInputState();
    }
  };

  resetInputState = () => {
    this.setState({
      value: '',
    });
  };

  handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      this.onSubmitForm(e);
    }
  };

  handleChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  renderTapMessage = () => (
    <span className="tap-message">{this.props.tapMessage}</span>
  );

  render() {
    const { value } = this.state;
    const { placeHolder, showTapMessage } = this.props;
    return (
      <>
        <label className="add-skills-input-label">
          <span>&#43;</span>
          <input
            type="text"
            ref={this.inputRef}
            placeholder={placeHolder}
            className="add-skills-input"
            value={value}
            onChange={this.handleChange}
            onKeyUp={this.handleKeyUp}
          />
        </label>

        {isDataExists(value) && showTapMessage && this.renderTapMessage()}
      </>
    );
  }
}
SkillForm.propTypes = {
  addItem: PropTypes.func,
  tapMessage: PropTypes.string,
  placeHolder: PropTypes.string,
  showTapMessage: PropTypes.bool,
};
SkillForm.defaultProps = {
  addItem: () => {},
  tapMessage: '',
  placeHolder: '',
  showTapMessage: true,
};
export default SkillForm;
