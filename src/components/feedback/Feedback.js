import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';
import Button from '../Button/Button';
import { addFeedBackAction } from '../../store/actions';
import 'react-toastify/dist/ReactToastify.css';
import icnClose from '../../assets/close-icn.svg';
import './feedback.scss';

const FEEDBACK_FORM_TITLE = 'Feedback';
const BTN_VALUE = 'Submit';

const Feedback = ({ user, addFeedBack }) => {
  const [showFeedbackOverlay, toggleFeedbackOverlay] = useState(false);
  const [description, updateDescription] = useState('');
  const handleFeedBackSubmit = () => {
    addFeedBack(description, user);
    toggleFeedbackOverlay(false);
    updateDescription('');
  };
  const renderFeedBackOverlay = () => (
    <div className="feedback-overlay">
      <div className="feedback-overlay-background" />
      <div className="form-section">
        <img
          src={icnClose}
          onClick={() => toggleFeedbackOverlay(false)}
          alt="close"
        />
        <p className="feedback-title">{FEEDBACK_FORM_TITLE}</p>
        <textarea
          onChange={({ target: { value } }) => updateDescription(value)}
          value={description}
        />
        <Button
          value={BTN_VALUE}
          disabled={!description.length}
          onClick={handleFeedBackSubmit}
        />
      </div>
    </div>
  );
  return (
    <>
      <div
        className="feed-back-container"
        title="feedback"
        onClick={() => toggleFeedbackOverlay(true)}
        role="button"
      >
        <span>?</span>
      </div>
      {showFeedbackOverlay && renderFeedBackOverlay()}
      <ToastContainer autoClose={1000} position="bottom-right" />
    </>
  );
};

Feedback.propTypes = {
  user: PropTypes.object.isRequired,
  addFeedBack: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
});

const mapDispatchToProps = (dispatch) => ({
  addFeedBack: (description, user) =>
    dispatch(addFeedBackAction(description, user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Feedback);
