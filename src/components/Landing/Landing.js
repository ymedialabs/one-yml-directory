/* eslint-disable jsx-a11y/control-has-associated-label */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import GoogleLogin from 'react-google-login';
import { withRouter } from 'react-router-dom';
import { createUser } from '../../store/actions';
import { getSession } from '../../services/AuthService';
import ymlLogoBackground from '../../assets/one-yml-logo.svg';
import ymlSquareLogo from '../../assets/yml-square-logo.png';
import signin from '../../assets/btn_google_signin_dark.png';
import './landing.scss';

const sectionstyle = {
  backgroundImage: `url(${ymlLogoBackground})`,
  backgroundPosition: 'left bottom',
  backgroundRepeat: 'no-repeat',
  backgroundSize: '1266px 424px',
};
const signinStyle = {
  backgroundImage: `url(${signin})`,
  backgroundSize: 'cover',
  backgroundColor: 'inherit',
};
class LandingPage extends React.Component {
  componentDidMount() {
    this.checkRedirection();
  }

  checkRedirection = () => {
    const { history } = this.props;
    getSession(history);
  };

  componentDidUpdate = () => {
    if (this.props.loggedUser) {
      this.props.history.push('/home');
    }
  };

  responseGoogle = (response) => {
    if (response && response.profileObj) {
      const userData = {
        name: response.profileObj.name,
        emailId: response.profileObj.email,
        avatarUrl: response.profileObj.imageUrl,
      };
      this.props.onAddUser(userData, this.props.history);
    }
  };

  getClientId = () =>
    process.env.OAUTH_CLIENT_ID ||
    '268770202825-ilpbu759ej9ab5oo4rvub2r05au2har6.apps.googleusercontent.com';

  render() {
    return (
      <div style={sectionstyle} className="landing-container">
        <div>
          <div className="center-screen">
            <div className="directory-container">
              <div>
                <img
                  className="yml-square-logo"
                  src={ymlSquareLogo}
                  alt="yml-square-logo"
                />
              </div>
              <p className="YML-directory">YML DIRECTORY</p>
              <GoogleLogin
                clientId={this.getClientId()}
                render={(renderProps) => (
                  <button
                    className="signin-btn"
                    style={signinStyle}
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                  />
                )}
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}
                cookiePolicy="single_host_origin"
                hostedDomain="ymedialabs.com"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LandingPage.propTypes = {
  onAddUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  loggedUser: state.auth.user,
});

const mapDispatchToProps = (dispatch) => ({
  onAddUser: (user, history) => {
    dispatch(createUser(user, history));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(LandingPage));
