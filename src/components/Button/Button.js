import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import './button.scss';

const Button = ({ size = 'medium', customClass, href, value, ...props }) => {
  const classes = cx([
    `variant-${props.type}`,
    `size-${size}`,
    `${customClass}`,
  ]);

  return href ? (
    <div>
      <a className={classes} href={href} {...props}>
        {value}
      </a>
    </div>
  ) : (
    <button
      type={props.type}
      className={`${classes} button`}
      {...props}
      value={value}
    >
      {value}
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  size: PropTypes.string,
  customClass: PropTypes.string,
  href: PropTypes.string,
  value: PropTypes.string.isRequired,
};

Button.defaultProps = {
  type: 'default',
  customClass: '',
  href: '',
  size: 'medium',
};
export default Button;
