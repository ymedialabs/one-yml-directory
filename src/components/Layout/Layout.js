import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Header from '../Header/Header';
import MobileHeader from '../MobileHeader/MobileHeader';
import Feedback from '../feedback/Feedback';
import ROUTE_CONSTANTS from '../../constants/routepath';
import './layout.scss';

const NAVIGATOR = {
  IPHONE: 'iphone',
  ANDROID: 'android',
};

const Layout = ({ user, children, history }) => {
  const handleIconClick = () => {
    history.push(ROUTE_CONSTANTS.DASHBOARD_ROUTE);
  };
  const handleVideoClick = () => {
    history.push(ROUTE_CONSTANTS.VIDEO_PAGE);
  };
  const checkMobileBrowser = () =>
    navigator.userAgent.toLowerCase().includes(NAVIGATOR.IPHONE) ||
    navigator.userAgent.toLowerCase().includes(NAVIGATOR.ANDROID);
  return (
    <div className="layout">
      {checkMobileBrowser() ? (
        <MobileHeader user={user} />
      ) : (
        <Header user={user} />
      )}
      <div className="main-component">{children}</div>
      <div
        className="chat-btn-container"
        onClick={handleIconClick}
        role="button"
      >
        <span className="dot" /> Chat
      </div>
      <div
        className="chat-btn-container video-btn-container"
        onClick={handleVideoClick}
        role="button"
      >
        <span className="dot" /> Video
      </div>
      <Feedback />
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
});
export default withRouter(connect(mapStateToProps)(Layout));
