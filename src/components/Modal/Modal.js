import React, { Component } from 'react';
import './modal.scss';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Popover } from 'antd';
import socket from '../../utils/socketConn';
import SearchBar from '../SearchBar/SearchBar';
import MemberList from '../Addmembers/MemberList';

class Modal extends Component {
  state = {
    groupMembers: [],
    currentRoom: {},
  };

  text = (<span>Existing Members</span>);

  getContent = () => (
    <div>
      {this.state.currentRoom.members.map((elem, index) => (
        <p key={index}>{elem}</p>
      ))}
    </div>
  );

  removeItem = (itemIndex) => {
    const { groupMembers } = { ...this.state };
    const tempMembers = [...groupMembers];
    tempMembers.splice(itemIndex, 1);
    this.setState({ groupMembers: tempMembers });
  };

  handleInputChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
  };

  handleSend = () => {
    // this.props.handleChatRoomState(this.state.groupName);
    socket.emit(
      'joinGroup',
      this.state.currentRoom.groupId,
      this.state.currentRoom.groupname,
      this.state.groupMembers
    );
    this.props.handleClose();
  };

  addMemberToGroup = (member) => {
    const memberList = [...this.state.currentRoom.members];
    const newMembers = [...this.state.groupMembers];
    if (
      !(memberList.includes(member.name) || newMembers.includes(member.name))
    ) {
      newMembers.push(member.name);
      this.setState({ groupMembers: newMembers });
    }
  };

  static getDerivedStateFromProps(nextProps) {
    return {
      currentRoom: nextProps.currentRoom,
    };
  }

  render() {
    return (
      <div className="popup">
        <div className="popup-inner">
          <h1 className="form-header">{this.props.text}</h1>
          <form id="popupform" onClick={this.handleSubmit}>
            <SearchBar
              users={this.props.users.userList}
              onClick={this.addMemberToGroup}
              customClass="custom-search-bar"
            />
            <MemberList
              items={this.state.groupMembers}
              removeItem={this.removeItem}
            />

            <button className="submit-group" onClick={() => this.handleSend()}>
              Send
            </button>
            <Popover
              placement="bottom"
              title={this.text}
              content={this.getContent()}
              trigger="click"
            >
              <button className="existing-members">Existing Members</button>
            </Popover>
          </form>
          <button
            className="close-btn popup-close-btn"
            onClick={() => this.props.handleClose()}
          >
            X
          </button>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  text: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  currentRoom: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
  user: state.auth.user,
});

export default withRouter(connect(mapStateToProps)(Modal));
