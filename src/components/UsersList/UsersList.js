import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const UsersList = ({ users }) => {
  if (!postMessage.length) {
    return <div>No Users Information</div>;
  }
  return <div>{users}</div>;
};

UsersList.propTypes = {
  users: PropTypes.node.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.posts,
});

export default connect(mapStateToProps)(UsersList);
