import React from 'react';
import Layout from '../Layout/Layout.js';
import logo from '../../assets/icn-logo3x.png';
import './pageNotFound.css';

const PAGE_NOT_FOUND_TEXT = '404. The Page you are looking for is not found';

const PageNotFound = () => (
  <Layout>
    <div className="no-data-found-container">
      <img src={logo} alt="logo" />
      <p className="no-data-found-text">{PAGE_NOT_FOUND_TEXT}</p>
    </div>
  </Layout>
);
export default PageNotFound;
