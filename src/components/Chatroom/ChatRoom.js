/* eslint-disable camelcase */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchAllUsers, getPrivateMessages } from '../../store/actions';
import './chatroom.css';

import ChatList from '../Chatlist/ChatList';
import Form from '../Form/Form';
import socket from '../../utils/socketConn';
import DataHandle from '../../utils/dataHandler';
import API_CONSTANTS from '../../constants/api';

class ChatRoom extends Component {
  state = {
    message: '',
    groupId: '',
    currentGroup: {},
    messageList: [],
  };

  handleEmojiAddition = (emoji) => {
    const { message } = { ...this.state };
    this.setState({
      message: message + emoji,
    });
  };

  handleChange = ({ target }) => {
    this.setState({ message: target.value });
  };

  sendGroupMessage = () => {
    if (this.state.message !== '') {
      socket.emit(
        'groupMessage',
        this.state.currentGroup,
        this.state.message,
        this.props.user
      );
    }
    this.setState({ message: '' });
  };

  handleCurrentGroupState = (data) => {
    this.setState({ currentGroup: data });
    DataHandle.getGroupMessages(
      API_CONSTANTS.GET_GROUP_MESSAGES,
      this.props.user.name,
      data.groupId,
      this.handleGroupMessageState
    );
  };

  handleGroupMessageState = (response) => {
    this.setState({ messageList: response });
  };

  componentDidMount = () => {
    const groupId = this.props.history.location.pathname.split('/');
    this.setState({ groupId: groupId[3] });
    DataHandle.getCurrentGroup(
      API_CONSTANTS.GET_CURRENT_GROUP,
      groupId[3],
      this.handleCurrentGroupState
    );
  };

  componentDidUpdate = (prevProps) => {
    const groupId = prevProps.history.location.pathname.split('/');
    if (this.state.groupId !== groupId[3]) {
      this.updateGroupId(groupId);
    }
  };

  updateGroupId = (groupId) => {
    this.setState({ groupId: groupId[3] });
    DataHandle.getCurrentGroup(
      API_CONSTANTS.GET_CURRENT_GROUP,
      groupId[3],
      this.handleCurrentGroupState
    );
  };

  UNSAFE_componentWillReceiveProps = (nextProps) => {
    const { messageList } = { ...this.state };
    const stateGroupMsg = [...messageList];
    let groupTexts = [];
    if (nextProps.grpMsg) {
      groupTexts = nextProps.grpMsg.filter(
        (elem) => elem.recieverId === this.state.groupId
      );
    }
    groupTexts.forEach((msg) => {
      const msgIndex = stateGroupMsg.findIndex(
        (eachMsg) =>
          eachMsg.sender === msg.sender && eachMsg.message === msg.message
      );
      if (msgIndex === -1) {
        stateGroupMsg.push(msg);
      }
    });
    this.setState({ messageList: stateGroupMsg });
  };

  render() {
    return (
      <div className="message-window">
        <div className="group-name">
          {this.state.currentGroup ? (
            <div className="group-name-header">
              {this.state.currentGroup.groupname}
            </div>
          ) : null}
        </div>
        <ChatList
          listText={this.state.messageList}
          currentUser={this.props.user}
        />
        <Form
          handleChange={this.handleChange}
          value={this.state.message}
          sendMessage={this.sendGroupMessage}
          handleEmojiAddition={this.handleEmojiAddition}
        />
      </div>
    );
  }
}

ChatRoom.propTypes = {
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  grpMsg: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
  user: state.auth.user,
});
const mapDispatchToProps = (dispatch) => ({
  fetchAllUsers: () => dispatch(fetchAllUsers()),
  getPrivateMessages: (url, senderId, recieverId) =>
    dispatch(getPrivateMessages(url, senderId, recieverId)),
});
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ChatRoom)
);
