import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import AddSkills from '../AddSkills/AddSkills';
import {
  fetchUserDetail,
  updateUser,
  fetchProjectResultsAction,
  addProjectInfoAction,
  updateProjectsArrayUserAction,
  resetSearchResultsAction,
  updateProjectsArray,
  getDepartmentListAction,
} from '../../store/actions';

import 'react-toastify/dist/ReactToastify.css';
import DropDown from '../DropDown/DropDown';
import './editProfile.scss';
import Input from '../Input/Input';
import Layout from '../Layout/Layout';
import UserIinitalComponent from '../UserIinitalComponent/UserIinitalComponent';
import NumberFormat from '../numberFormat/NumberFormat.js';
import DatePickerComponent from '../DatePickerComponent/DatePickerComponent';
import Button from '../Button/Button';
import ProjectsUpload from '../ProjectsUpload/ProjectsUpload';
import * as ROUTES from '../../constants/routes';
import { isDataExists } from '../../utils/utils';
import FEATURES_INFO from '../../accessRestrictions';
import {
  getSessioninfo,
  checkUserAccessFeature,
} from '../../services/AuthService';

class EditProfilePage extends React.Component {
  state = {
    errors: {},
    formData: {
      employeeId: '',
      designation: '',
      phoneNumber: '',
      dob: '',
      department: '',
      skills: [],
    },
    skillDisplayStatus: false,
    submitDisable: true,
    adminChecked: false,
  };

  componentDidMount() {
    this.getProfileDetails();
    this.props.getDepartmentList();
  }

  checkIfAdmin = () => this.props.currentUser.roles.includes('admin');

  handleAdminState = () => {
    this.setState({ adminChecked: this.checkIfAdmin() });
  };

  componentDidUpdate(prevProps) {
    if (this.props.user._id && prevProps.user._id !== this.props.user._id) {
      this.getProfileDetails();
    }
    if (
      prevProps.currentUser !== this.props.currentUser &&
      Object.keys(this.props.currentUser).length > 0
    ) {
      this.setInitialState();
    }
    if (
      prevProps.projectInfo &&
      this.props.projectInfo &&
      prevProps.projectInfo.length !== this.props.projectInfo.length
    ) {
      this.enableBtn();
    }
  }

  getProfileDetails = () => {
    const key = this.props.user._id;
    const userId = this.props.location.pathname.split('/');
    if (key) {
      this.props.fetchUser(userId[2]);
    }
  };

  setInitialState = () => {
    const { currentUser } = this.props;
    const { formData } = { ...this.state };
    const formDataSet = { ...formData };
    Object.keys(currentUser).forEach((key) => {
      formDataSet[key] = currentUser[key];
    });
    this.setState({ formData: formDataSet, submitDisable: true });
  };

  handleInputChange = (value, name) => {
    const formData = { ...this.state.formData };
    this.setState({
      formData: {
        ...formData,
        [name]: value,
      },
      submitDisable: false,
    });
    this.setState({ errors: {} });
    this.handleAdminState();
  };

  enableBtn = () => {
    this.setState({ submitDisable: false });
  };

  handleFormValidation() {
    const fields = this.state.formData;
    const errors = {};
    let formIsValid = true;

    // Designation
    if (typeof fields.designation !== 'undefined') {
      if (!fields.designation.match(/^[a-zA-Z ]+$/)) {
        formIsValid = false;
        errors.designation = 'Enter valid Designation';
      }
    }

    // Phone Number
    if (typeof fields.phoneNumber !== 'undefined') {
      if (
        !fields.phoneNumber.match(
          /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
        )
      ) {
        formIsValid = false;
        errors.phoneNumber = 'Enter valid Phone Number.';
      }
    }
    // Employee ID
    if (fields.employeeId === '') {
      formIsValid = false;
      errors.employeeId = 'Enter valid Employee ID';
    }

    // Date
    if (fields.dob === '') {
      formIsValid = false;
      errors.dob = 'Enter valid Date of Birth.';
    }

    // Department
    if (fields.department === '') {
      formIsValid = false;
      errors.department = 'Please select department.';
    }

    this.setState({ errors });
    return formIsValid;
  }

  onKeyPress = (e) => {
    if (e.key === 'Enter') e.preventDefault();
  };

  handleRoleUpdate = (userRoles) => {
    if (this.state.adminChecked && !userRoles.includes('admin')) {
      userRoles.push('admin');
    }
    if (!this.state.adminChecked && userRoles.includes('admin')) {
      userRoles = [...userRoles].filter((role) => role !== 'admin');
    }
    return userRoles;
  };

  routeToProfile = () => {
    const { history } = this.props;
    history.push(ROUTES.PROFILE);
  };

  onSubmit = (e) => {
    e.preventDefault();
    const { projectInfo } = this.props;
    let userObject = this.state.formData;
    const { roles } = this.props.currentUser;
    const userRoles = this.handleRoleUpdate(roles);
    userObject = { ...userObject, projectInfo, roles: userRoles };
    const userId = this.props.location.pathname.split('/');
    const loggedUserInfo = getSessioninfo();
    if (this.handleFormValidation()) {
      this.props.updateUser(userId[2], userObject, loggedUserInfo._id);
      this.setState({ submitDisable: true });
      setTimeout(() => this.routeToProfile(), 2000);
    }
  };

  handleCancel = () => {
    const { history } = this.props;
    history.push(ROUTES.PROFILE);
  };

  handleProjectAddChange = (value) => {
    const { fetchProjectResults } = this.props;
    if (value && value.length > 2) {
      fetchProjectResults(value);
    }
  };

  handleKeyPressProject = (value) => {
    const {
      addProjectInfo,
      projectSearchResults,
      updateProjectsArrayUser,
    } = this.props;
    const projectSelected = projectSearchResults.find(
      (elem) => elem.name === value
    );
    if (projectSelected) {
      updateProjectsArrayUser(projectSelected);
    } else if (isDataExists(value)) {
      addProjectInfo(value);
    }
  };

  handleInputChangeproject = (value, elem, fromClose = false) => {
    const { projectInfo, updateProjectsArrayAction } = this.props;
    const newProjectArr = [...projectInfo];
    const index = [...projectInfo].findIndex(
      (entity) => entity.name === elem.name
    );
    if (!fromClose) {
      newProjectArr.splice(index, 1, { ...elem, isComplete: value });
    } else {
      newProjectArr.splice(index, 1);
    }
    updateProjectsArrayAction(newProjectArr);
    this.enableBtn();
    this.handleAdminState();
  };

  handleAdminClick = (event) => {
    this.setState({ adminChecked: event.target.checked });
    this.enableBtn();
  };

  renderAdminCheckBox = () => (
    <div>
      <label className="container">
        Admin
        <input
          type="checkbox"
          defaultChecked={this.checkIfAdmin()}
          onChange={this.handleAdminClick}
        />
        <div className="admin-checkmark" />
      </label>
    </div>
  );

  render() {
    const user = this.props.currentUser;
    const { projectInfo, projectSearchResults, departmentList } = this.props;
    return (
      <Layout>
        {user && Object.keys(user).length > 0 && (
          <div className="profile-body">
            <div className="profile-container">
              <div className="edit-profile-user-details">
                {user.avatarUrl ? (
                  <img
                    className="profile-photo"
                    src={user.avatarUrl}
                    alt="user-profile"
                  />
                ) : (
                  <UserIinitalComponent
                    className="profile-user-image"
                    name={user.name}
                  />
                )}
                <div className="profile-name-designation">
                  <div className="edit-profile-name">{user.name}</div>
                  <div className="profile-designation">
                    <Input
                      type="text"
                      name="designation"
                      placeholder="Designation"
                      value={user.designation}
                      onKeyPress={this.onKeyPress}
                      onChange={this.handleInputChange}
                    />
                    <p className="error-msg">{this.state.errors.designation}</p>
                  </div>
                </div>
                {checkUserAccessFeature(
                  FEATURES_INFO.EDIT_PROFILE.ADMIN_ROLE
                ) && this.renderAdminCheckBox()}
              </div>
              <div className="edit-profile-email-phone">
                <p className="contact-info">CONTACT INFO</p>
                <div className="profile-email">
                  <i className="fa fa-envelope-o" />
                  <p className="user-email">{user.emailId}</p>
                </div>
                <div className="profile-phone">
                  <i className="fa fa-phone" />
                  <div className="phone-number-input">
                    <NumberFormat
                      name="phoneNumber"
                      placeholder="Phone number"
                      handleKeyPress={this.onKeyPress}
                      handleChange={this.handleInputChange}
                      customClass="input-field-profile"
                      inputVal={user.phoneNumber}
                    />
                    <p className="error-msg">{this.state.errors.phoneNumber}</p>
                  </div>
                </div>
              </div>
              <div className="edit-profile-id-dob-dept">
                <div className="id-dob-dept">
                  <label className="profile-label">EMPLOYEE ID</label>
                  <div>
                    {user.employeeId ? (
                      <p className="profile-user-employeeid">
                        {user.employeeId}
                      </p>
                    ) : (
                      <Input
                        type="text"
                        name="employeeId"
                        placeholder="YML0000"
                        value={user.employeeId}
                        onKeyPress={this.onKeyPress}
                        onChange={this.handleInputChange}
                      />
                    )}
                    <p className="error-msg">{this.state.errors.employeeId}</p>
                  </div>
                </div>
                <div className="id-dob-dept">
                  <label className="profile-label">BIRTHDAY</label>
                  <div>
                    <DatePickerComponent
                      handleChange={(value) => {
                        this.handleInputChange(value, 'dob');
                      }}
                      placeholderText="MM-DD-YYYY"
                      dateValue={user.dob}
                      customClass="input-field-profile date-picker-profile"
                      calendarClassName="profile-date-picker"
                      enableBtn={this.enableBtn}
                    />
                    <p className="error-msg">{this.state.errors.dob}</p>
                  </div>
                </div>
                <div className="id-dob-dept">
                  <label className="profile-label">DEPARTMENT</label>
                  <div className="center">
                    <DropDown
                      name="department"
                      onKeyPress={this.onKeyPress}
                      value={user.department}
                      placeholder="Select a department"
                      onChange={this.handleInputChange}
                      data={departmentList}
                    />
                    <p className="error-msg">{this.state.errors.department}</p>
                  </div>
                </div>
              </div>
              <div>
                <p className="skills-header">SKILLS</p>
                <AddSkills
                  skills={user.skills}
                  customClass="skills-list-edit"
                  skillDisplayStatus={this.state.skillDisplayStatus}
                  enableBtn={this.enableBtn}
                  handleAdminState={this.handleAdminState}
                />
              </div>
              <div>
                <p className="skills-header margin-class">PROJECTS</p>
                <ProjectsUpload
                  projectsList={projectInfo}
                  handleChange={this.handleProjectAddChange}
                  projectSearchResults={projectSearchResults}
                  handleKeyPressProject={this.handleKeyPressProject}
                  handleInputChange={this.handleInputChangeproject}
                />
              </div>
              <div className="save-cancel">
                <Button
                  value="Cancel"
                  customClass="cancel-btn"
                  onClick={this.handleCancel}
                />
                <Button
                  value="Save"
                  customClass="save-btn"
                  disabled={this.state.submitDisable}
                  onClick={this.onSubmit}
                />
              </div>
            </div>
          </div>
        )}
        <ToastContainer autoClose={1000} position="bottom-right" />
      </Layout>
    );
  }
}

EditProfilePage.propTypes = {
  currentUser: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
    .isRequired,
  user: PropTypes.object.isRequired,
  updateUser: PropTypes.func.isRequired,
  fetchUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  projectInfo: PropTypes.array.isRequired,
  fetchProjectResults: PropTypes.func.isRequired,
  projectSearchResults: PropTypes.array.isRequired,
  addProjectInfo: PropTypes.func.isRequired,
  updateProjectsArrayUser: PropTypes.func.isRequired,
  updateProjectsArrayAction: PropTypes.func.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }),
  getDepartmentList: PropTypes.func.isRequired,
  departmentList: PropTypes.array.isRequired,
};

EditProfilePage.defaultProps = {
  location: {
    pathname: '',
  },
};

const mapStateToProps = (state) => ({
  currentUser: state.users.userData,
  user: state.auth.user,
  projectInfo: state.users.projectInfo,
  projectSearchResults: state.users.projectSearchResults,
  departmentList: state.users.departmentList,
});

const mapDispatchToProps = (dispatch) => ({
  updateUser: (id, user, loggedUser) =>
    dispatch(updateUser(id, user, loggedUser)),
  fetchUser: (id) => dispatch(fetchUserDetail(id)),
  fetchProjectResults: (value) => dispatch(fetchProjectResultsAction(value)),
  addProjectInfo: (value) => dispatch(addProjectInfoAction(value)),
  updateProjectsArrayUser: (projectSelected) =>
    dispatch(updateProjectsArrayUserAction(projectSelected)),
  resetSearchResults: () => dispatch(resetSearchResultsAction()),
  updateProjectsArrayAction: (arr) => dispatch(updateProjectsArray(arr)),
  getDepartmentList: () => dispatch(getDepartmentListAction()),
});
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(EditProfilePage)
);
