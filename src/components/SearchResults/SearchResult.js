import React from 'react';
import PropTypes from 'prop-types';

const SearchResult = ({ addMemberToGroup, user: { name }, ...props }) => (
  <div className="seach-result-user-card" {...props}>
    <div
      className="search-user-name"
      onClick={() => addMemberToGroup()}
      role="button"
    >
      {name}
    </div>
  </div>
);

SearchResult.propTypes = {
  addMemberToGroup: PropTypes.func,
  user: PropTypes.object.isRequired,
};

SearchResult.defaultProps = {
  addMemberToGroup: () => {},
};

export default SearchResult;
