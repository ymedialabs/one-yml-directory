/* eslint-disable camelcase */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { Icon } from 'antd';
import { fetchAllUsers } from '../../store/actions';
import './sidebar.scss';
import PopUpForm from '../Popupform/PopUpForm';
import Modal from '../Modal/Modal';
import SearchBar from '../SearchBar/SearchBar';
import ROUTE_CONSTANTS from '../../constants/routepath';
import API_CONSTANTS from '../../constants/api';
import DataHandle from '../../utils/dataHandler';

class SideBar extends Component {
  state = {
    showPopup: false,
    chatRooms: [],
    showModal: false,
    currentRoom: {},
  };

  onlineUsersData = [];

  renderListOfUsers = (user, index) => (
    <NavLink
      className="link-elem"
      activeClassName="active-link-card"
      to={`${this.props.match.path}/${user._id}`}
      key={index}
    >
      <div>
        <img
          className="sidebar-user-profile-image"
          src={user.avatarUrl}
          alt="user-profile"
        />
      </div>
      <div className="sidebar-user-profile-name">{user.name}</div>
    </NavLink>
  );

  handleChatRoomState = (data) => {
    this.setState({ chatRooms: data });
  };

  handlePrivateWindowRoute = (user) => {
    this.props.history.push(`${this.props.match.path}/${user._id}`);
  };

  handlePopUpDisplay = () => {
    this.setState({ showPopup: true });
  };

  handleModalClose = () => {
    this.setState({ showModal: false });
  };

  handleClose = () => {
    this.setState({ showPopup: false });
  };

  handleDashboardRoute = () => {
    this.props.history.push(ROUTE_CONSTANTS.DASHBOARD_ROUTE);
  };

  componentDidMount = () => {
    this.props.fetchAllUsers();

    DataHandle.getgroupList(
      API_CONSTANTS.GET_GROUP_LIST,
      this.handleChatRoomState
    );
  };

  inviteToGroup = (room) => {
    this.setState({ currentRoom: room, showModal: true });
  };

  UNSAFE_componentWillReceiveProps = (nextProps) => {
    const { chatRooms } = { ...this.state };
    const tempRooms = [...chatRooms];
    if (
      Object.keys(nextProps.roomData).length !==
      Object.keys(this.props.roomData).length
    ) {
      tempRooms.push(nextProps.roomData);
    }
    this.setState({ chatRooms: tempRooms });
  };

  componentDidUpdate = (prevProps) => {
    const listOfUsers = prevProps.userList;
    const onlineUsers = [];
    listOfUsers.forEach((elem) => {
      this.props.users.forEach((user) => {
        if (user.name === elem) {
          onlineUsers.push(user);
          this.onlineUsersData.push(user);
        }
      });
    });
    if (prevProps.userList !== this.props.userList) {
      this.setOnlineUsers(onlineUsers);
    }
  };

  setOnlineUsers = (onlineUsers) => {
    this.setState({ onlineUsers });
  };

  render() {
    return (
      <div>
        <div className="sidebar">
          <SearchBar
            users={this.props.users}
            onClick={this.handlePrivateWindowRoute}
          />
          <p className="groups-text">Message</p>
          <ul className="user-list">
            {this.props.userInfo.map((user, index) =>
              this.renderListOfUsers(user, index)
            )}
          </ul>
          <p className="groups-text">Groups</p>
          <ul className="user-list group-list">
            {this.state.chatRooms &&
              this.state.chatRooms.map((room, index) =>
                room.members.includes(this.props.user.name) ? (
                  <li key={index} className="link-elem">
                    <Icon
                      type="user-add"
                      className="user-add-icon"
                      onClick={() => this.inviteToGroup(room)}
                    />
                    <NavLink
                      className="navlink-element"
                      activeClassName="nav-active-link"
                      to={`${this.props.match.path}/group/${room.groupId}`}
                      key={index}
                    >
                      {room.groupname}
                    </NavLink>
                  </li>
                ) : null
              )}
          </ul>
          <div className="dashboard-icon">
            <Icon
              type="home"
              className="broadcast-icon"
              onClick={this.handleDashboardRoute}
            />
            <Icon
              type="usergroup-add"
              className="group-create-icon"
              onClick={this.handlePopUpDisplay}
            />
          </div>
        </div>

        {this.state.showPopup && (
          <PopUpForm text="Create Group" handleClose={this.handleClose} />
        )}
        {this.state.showModal && (
          <Modal
            text={`Add Member to ${this.state.currentRoom.groupname}`}
            currentRoom={this.state.currentRoom}
            handleClose={this.handleModalClose}
          />
        )}
      </div>
    );
  }
}

SideBar.propTypes = {
  users: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  userInfo: PropTypes.array.isRequired,
  match: PropTypes.object.isRequired,
  userList: PropTypes.array.isRequired,
  fetchAllUsers: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  roomData: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users.userList,
  user: state.auth.user,
});
const mapDispatchToProps = (dispatch) => ({
  fetchAllUsers: () => dispatch(fetchAllUsers()),
});
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SideBar)
);
