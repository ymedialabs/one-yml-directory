import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import './userCard.scss';
import '../AddSkills/addSkills.scss';
import UserIinitalComponent from '../UserIinitalComponent/UserIinitalComponent';
import PopOver from '../PopOver/PopOver';
import { isDataExists } from '../../utils/utils';

const ITEMS_TO_SHOW = 2;

const UserCard = ({
  user: { _id, name, designation, avatarUrl, projectInfo },
  index,
  ...props
}) => {
  const [showPopUp, togglePopUp] = useState(false);
  const renderImageContainer = () =>
    avatarUrl ? (
      <img src={avatarUrl} alt="user-profile" />
    ) : (
      <UserIinitalComponent
        className="user-initial-display-container"
        name={name}
      />
    );

  const getCurrentProjects = () =>
    projectInfo.filter((elem) => !elem.isComplete);

  const renderCurrentProjects = (arr) =>
    [...arr].map((elem, projectIndex) => (
      <div key={projectIndex} className="current-project">
        <span className="current-dot" />
        <span className="project-name">{elem.name}</span>
      </div>
    ));

  const getRemainingLengthText = () => (
    <span className="additional-skills-text">
      {`+ ${getCurrentProjects().length - ITEMS_TO_SHOW}`}
    </span>
  );

  const getPopUpChildrenArray = () => [
    ...getCurrentProjects().slice(ITEMS_TO_SHOW, getCurrentProjects().length),
  ];

  const renderPopUpChildren = () =>
    [...getPopUpChildrenArray()].map((item, projIndex) => (
      <li key={projIndex}>
        <span>{item.name}</span>
        {projIndex !== [...getPopUpChildrenArray()].length - 1 && (
          <span>,</span>
        )}
      </li>
    ));

  const renderPopUp = () => (
    <PopOver
      customClass="skills-pop-over"
      handleOutsideClick={() => {
        togglePopUp(false);
      }}
    >
      <ul>{renderPopUpChildren()}</ul>
    </PopOver>
  );

  const renderProfileSkills = () => {
    const displaySkills = [...getCurrentProjects()];
    if (isDataExists(displaySkills, ITEMS_TO_SHOW)) {
      displaySkills.splice(ITEMS_TO_SHOW, getCurrentProjects().length);
    }
    return (
      <>
        {renderCurrentProjects(displaySkills)}
        {isDataExists(getCurrentProjects(), ITEMS_TO_SHOW) && (
          <div
            onClick={() => {
              togglePopUp(!showPopUp);
            }}
            className="skills-list-additional"
            role="button"
          >
            {getRemainingLengthText()}
            {showPopUp && renderPopUp()}
          </div>
        )}
      </>
    );
  };

  const renderProjectInfoContainer = () => (
    <>
      {isDataExists(getCurrentProjects()) && (
        <div className="current-project-section skills-list">
          {renderProfileSkills()}
        </div>
      )}
    </>
  );
  const renderNotStartedComponent = () => (
    <p className="not-started-text">Not yet started</p>
  );
  return (
    <div className="user-card-container" {...props}>
      <NavLink className="cards" to={`/user/${_id}?index=${index + 1}`}>
        <div className="image-and-name">
          <div className="user-name-desig">
            <p className="user-name">{name}</p>
            {designation && <p className="user-designation">{designation}</p>}
          </div>
          {renderImageContainer()}
        </div>
        <div className="grey-border" />
        <p className="current-projects-text">Current Projects</p>
      </NavLink>
      {isDataExists(getCurrentProjects())
        ? renderProjectInfoContainer()
        : renderNotStartedComponent()}
    </div>
  );
};

UserCard.propTypes = {
  user: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
};

export default UserCard;
