import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchUserDetail } from '../../store/actions';
import Layout from '../Layout/Layout';
import SkillsList from '../AddSkills/SkillsList';
import UserIinitalComponent from '../UserIinitalComponent/UserIinitalComponent';
import { showDateInFormat, isDataExists } from '../../utils/utils';
import './profile.scss';
import '../UserInfo/userInfo.scss';

class Profile extends React.Component {
  state = {
    skillDisplayStatus: true,
  };

  componentDidMount() {
    this.getProfileDetails();
  }

  componentDidUpdate(prevProps) {
    if (this.props.user._id && prevProps.user._id !== this.props.user._id) {
      this.getProfileDetails();
    }
  }

  getProfileDetails = () => {
    const key = this.props.user._id;
    if (key) {
      this.props.fetchUser(key);
    }
  };

  renderImageContainer = () =>
    this.props.user.avatarUrl ? (
      <img
        className="profile-photo"
        src={this.props.user.avatarUrl}
        alt="user-profile"
      />
    ) : (
      <UserIinitalComponent
        className="profile-user-image"
        name={this.props.user.name}
      />
    );

  getCurrentProjects = () =>
    isDataExists(this.props.currentUser.projectInfo) &&
    [...this.props.currentUser.projectInfo].filter((elem) => !elem.isComplete);

  getCompletedProjects = () =>
    isDataExists(this.props.currentUser.projectInfo) &&
    [...this.props.currentUser.projectInfo].filter((elem) => elem.isComplete);

  render() {
    const {
      currentUser: {
        name,
        designation,
        emailId,
        phoneNumber,
        employeeId,
        skills,
        department,
        dob,
      },
    } = this.props;
    return (
      <Layout>
        <div className="profile-body">
          <div className="profile-container">
            <div className="user-profile-info">
              <div className="user-details">
                <div className="profile-image-name-designation">
                  {this.renderImageContainer()}
                  <div className="profile-name-designation">
                    <div className="profile-name">{name}</div>
                    <div className="profile-designation">{designation}</div>
                    <Link to={`/edit-profile/${this.props.user._id}`}>
                      <p className="edit-btn">Edit profile</p>
                    </Link>
                  </div>
                </div>
              </div>

              <div className="edit-profile-email-phone">
                <p className="contact-info">CONTACT INFO</p>
                <div className="profile-email">
                  <i className="fa fa-envelope-o" />
                  <p className="profile-emailid">{emailId}</p>
                </div>
                <div className="profile-phone">
                  <i className="fa fa-phone" />
                  <p className="phone-number"> {phoneNumber}</p>
                </div>
              </div>
              <div className="edit-profile-id-dob-dept">
                <div>
                  <label className="profile-label">EMPLOYEE ID</label>
                  <div className="id-dob-dept">{employeeId}</div>
                </div>
                <div>
                  <label className="profile-label">BIRTHDAY</label>
                  <div className="id-dob-dept">
                    {dob && showDateInFormat(dob)}
                  </div>
                </div>
                <div>
                  <label className="profile-label">DEPARTMENT</label>
                  <div className="id-dob-dept">{department}</div>
                </div>
              </div>
              <div>
                <p className="profile-skills-header">SKILLS</p>
                {isDataExists(skills) && (
                  <SkillsList
                    items={skills}
                    skillDisplayStatus={this.state.skillDisplayStatus}
                  />
                )}
              </div>
              <div className="projects-listing-section">
                <div className="current-projects">
                  {isDataExists(this.getCurrentProjects()) && (
                    <>
                      <p className="profile-skills-header">CURRENT PROJECTS</p>
                      <ul>
                        {this.getCurrentProjects().map((elem) => (
                          <li key={elem._id}>{elem.name}</li>
                        ))}
                      </ul>
                    </>
                  )}
                </div>
                <div className="completed-projects">
                  {isDataExists(this.getCompletedProjects()) && (
                    <>
                      <p className="profile-skills-header">
                        COMPLETED PROJECTS
                      </p>
                      <ul>
                        {this.getCompletedProjects().map((elem) => (
                          <li key={elem._id}>{elem.name}</li>
                        ))}
                      </ul>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Profile.propTypes = {
  currentUser: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    .isRequired,
  user: PropTypes.object.isRequired,
  fetchUser: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  currentUser: state.users.userData,
  user: state.auth.user,
});

const mapDispatchToProps = (dispatch) => ({
  fetchUser: (id) => dispatch(fetchUserDetail(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
