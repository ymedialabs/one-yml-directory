import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import { fetchSearchUser } from '../../store/actions';
import './headersearch.scss';
import HeaderSearchResult from '../HeaderSearchResult/HeaderSearchResult';

class HeaderSearchBar extends Component {
  state = {
    searchString: '',
  };

  handleChange = (e) => {
    this.setState({ searchString: e.target.value });
    this.getSearchResult(this.state.searchString);
  };

  getSearchResult = (query) => {
    this.props.fetchSearchUser(query);
  };

  closeSearchResults = () => {
    this.setState({
      searchString: '',
    });
  };

  getContainerClass = () => cx(['header-search-bar', this.props.customClass]);

  render() {
    const { placeHolder } = this.props;
    const { searchString } = this.state;
    return (
      <div className={this.getContainerClass()}>
        <form
          onSubmit={(e) => {
            e.preventDefault();
          }}
          autoComplete="off"
        >
          <i className="fa fa-search" />
          <input
            className="header-searchbar-input"
            type="search"
            id="filter"
            placeholder={placeHolder}
            value={this.state.searchString}
            onChange={this.handleChange}
          />
        </form>

        {searchString && (
          <div className="header-search-result">
            <HeaderSearchResult closeSearchResults={this.closeSearchResults} />
          </div>
        )}
      </div>
    );
  }
}
HeaderSearchBar.propTypes = {
  fetchSearchUser: PropTypes.func.isRequired,
  placeHolder: PropTypes.string,
  customClass: PropTypes.string,
};

HeaderSearchBar.defaultProps = {
  placeHolder: 'Search by name or skills...',
  customClass: '',
};

const mapDispatchToProps = (dispatch) => ({
  fetchSearchUser: (data) => dispatch(fetchSearchUser(data)),
});
export default connect(null, mapDispatchToProps)(HeaderSearchBar);
