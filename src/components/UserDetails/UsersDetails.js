import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import UserInfo from '../UserInfo/UserInfo';
import { fetchUserDetail } from '../../store/actions';
import Layout from '../Layout/Layout';
import Loader from '../Loader/Loader';

class UserDetails extends React.Component {
  componentDidMount() {
    const {
      params: { id },
    } = this.props.match;
    this.props.fetchUser(id);
  }

  componentDidUpdate(prevProps) {
    const {
      params: { id },
    } = this.props.match;
    if (prevProps.match.params.id !== id) {
      this.props.fetchUser(id);
    }
  }

  getUserInfo = () => <UserInfo user={this.props.users.userData} />;

  render() {
    const { showLoader } = this.props.users;
    return (
      <Layout>
        {showLoader && <Loader />}
        <div className="profile-body">{this.getUserInfo()}</div>
      </Layout>
    );
  }
}

UserDetails.propTypes = {
  fetchUser: PropTypes.func.isRequired,
  users: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
});
const mapDispatchToProps = (dispatch) => ({
  fetchUser: (id) => dispatch(fetchUserDetail(id)),
});
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserDetails)
);
