import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../../store/actions';

import * as ROUTES from '../../constants/routes';
import Button from '../Button/Button';
import ModalComponent from '../ModalComponent/ModalComponent';
import ymlLogo from '../../assets/icn-logo3x.png';
import 'antd/dist/antd.css';
import './profileSidebar.scss';

class ProfileSidebar extends Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.logout();
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  logout = () => {
    this.setState({ visible: false }, () => {
      this.props.logoutUser(this.props.history);
    });
  };

  getBtnConfiguration = () => [
    {
      value: 'Cancel',
      handler: this.handleCancel,
      disabled: false,
      customClass: 'logout-modal-cancel',
    },
    {
      value: 'Log out',
      handler: this.logout,
      disabled: false,
      customClass: 'logout-btn',
    },
  ];

  renderActionContainer = () =>
    this.getBtnConfiguration().map((btn, index) => (
      <Button
        key={index}
        value={btn.value}
        onClick={btn.handler}
        disabled={btn.disabled}
        customClass={btn.customClass}
      />
    ));

  renderModalChildren = () => (
    <div className="logout-modal">
      <p>Do you want to log out?</p>
      <div className="action-container">{this.renderActionContainer()}</div>
    </div>
  );

  renderModal = () => (
    <ModalComponent>{this.renderModalChildren()}</ModalComponent>
  );

  render() {
    const {
      user: { name, emailId, avatarUrl },
    } = this.props;
    const { visible } = this.state;
    return (
      <div className="profile-sidebar-container">
        <Link to={ROUTES.HOME}>
          <img src={ymlLogo} className="sidebar-yml-logo" alt="YML-logo" />
        </Link>
        <div className="sidebar-profile-image-container">
          <img
            className="sidebar-profile-image"
            src={avatarUrl}
            alt="user-profile"
          />
        </div>
        <div className="sidebar-profile-name">{name}</div>
        <div className="sidebar-profile-email">{emailId}</div>
        <button
          className="profile-sidebar-logout"
          type="button"
          onClick={this.showModal}
        >
          <i className="fa fa-sign-out" aria-hidden="true" />
        </button>
        {visible && this.renderModal()}
      </div>
    );
  }
}

ProfileSidebar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  logoutUser: (history) => dispatch(logoutUser(history)),
});
export default connect(null, mapDispatchToProps)(withRouter(ProfileSidebar));
