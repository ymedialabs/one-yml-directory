import React from 'react';
import PropTypes from 'prop-types';
import './search.scss';

class SearchBar extends React.Component {
  state = {
    searchString: '',
  };

  handleChange = (e) => {
    this.setState({ searchString: e.target.value });
  };

  handleClick = (user) => {
    this.props.onClick(user);
    this.setState({ searchString: '' });
  };

  getSearchResult = () => {
    let userList = this.props.users;
    const searchString = this.state.searchString.trim().toLowerCase();

    if (searchString.length > 0) {
      userList = userList.filter((index) =>
        index.name.toLowerCase().match(searchString)
      );
    }
    if (userList.length !== this.props.users.length) {
      return userList.map((user, i) => (
        <div className="seach-result-user-card" key={i}>
          <div
            className="search-user-name"
            onClick={() => this.handleClick(user)}
            role="button"
          >
            {user.name}
          </div>
        </div>
      ));
    }
  };

  render() {
    return (
      <div className="search-input">
        {/* <i className="fa fa-search"></i> */}
        <input
          className={`${this.props.customClass} searchbar-input`}
          type="text"
          id="filter"
          placeholder="Search to chat..."
          value={this.state.searchString}
          onChange={this.handleChange}
        />
        <div className="search-result">{this.getSearchResult()}</div>
      </div>
    );
  }
}

SearchBar.propTypes = {
  customClass: PropTypes.string,
  users: PropTypes.array.isRequired,
  onClick: PropTypes.func,
};

SearchBar.defaultProps = {
  customClass: '',
  onClick: () => {},
};
export default SearchBar;
