import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '../../store';
import HandleRoute from '../HandleRoute/HandleRoute';
import '../../styles/_main.scss';

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <HandleRoute />
      </Switch>
    </Router>
  </Provider>
);

export default App;
