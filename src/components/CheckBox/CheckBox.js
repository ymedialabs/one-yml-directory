import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import closeIcn from '../../assets/close-icn-blue.svg';
import selectedIcn from '../../assets/selectedImg.svg';
import './checkbox.scss';

const CheckBox = ({
  label,
  inputValue,
  customClass,
  handleClose,
  type,
  showClose,
  showCompletedText,
  handleInputChange,
  ...props
}) => {
  const [val, setVal] = useState(inputValue);
  useEffect(() => {
    if (val !== inputValue) {
      handleInputChange(val);
    }
  }, [setVal, inputValue, val, handleInputChange]);
  const getContainerClass = () => cx('checkbox-container', customClass);
  const getSelectedClass = () => cx({ selected: val });
  return (
    <div className={getContainerClass()} {...props}>
      <label>{label}</label>
      {val && (
        <img
          src={selectedIcn}
          alt="check"
          className="check-icn"
          onClick={() => {
            setVal(!val);
          }}
        />
      )}
      <input
        type={type}
        value={val}
        onChange={() => {
          setVal(!val);
        }}
        className={getSelectedClass()}
      />
      <span className="checkmark" />
      {showCompletedText && <span>Completed</span>}
      {showClose && (
        <img
          src={closeIcn}
          alt="close"
          onClick={() => {
            handleClose(val);
          }}
        />
      )}
    </div>
  );
};

CheckBox.propTypes = {
  label: PropTypes.string,
  inputValue: PropTypes.bool,
  customClass: PropTypes.string,
  handleClose: PropTypes.func,
  type: PropTypes.string,
  showClose: PropTypes.bool,
  showCompletedText: PropTypes.bool,
  handleInputChange: PropTypes.func,
};

CheckBox.defaultProps = {
  label: '',
  inputValue: '',
  customClass: '',
  handleClose: () => {},
  type: 'checkbox',
  showClose: false,
  showCompletedText: false,
  handleInputChange: () => {},
};
export default CheckBox;
