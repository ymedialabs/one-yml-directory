import React from 'react';
import loaderImg from '../../assets/Loading.gif';
import './loader.scss';

const Loader = () => (
  <div className="loading-container">
    <div className="loader-background" />
    <img src={loaderImg} alt="loader" />
  </div>
);

export default Loader;
