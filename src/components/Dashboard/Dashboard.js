import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ToastContainer, toast } from 'react-toastify';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';

import { withRouter, Switch, Route } from 'react-router-dom';
import { notification } from 'antd';
import ProfileSidebar from '../ProfileSidebar/ProfileSidebar';
import socket from '../../utils/socketConn';
import './dashboard.scss';
import Form from '../Form/Form';
import SideBar from '../Sidebar/SideBar';
import ChatList from '../Chatlist/ChatList';
import PrivateDashboard from '../Privatedashboard/PrivateDashboard';
import ChatRoom from '../Chatroom/ChatRoom';
import DataHandle from '../../utils/dataHandler';
import API_CONSTANTS from '../../constants/api';

class Dashboard extends Component {
  state = {
    messageList: [],
    message: '',
    userList: [],
    userInformation: [],
    privateMsgs: [],
    conversations: [],
    roomData: {},
    groupMsg: [],
  };

  openNotification = (groupName) => {
    notification.open({
      message: 'Group Created',
      description: `${groupName} group created. `,
      duration: 1,
      onClick: () => {},
    });
  };

  handleMessagestate = () => {
    this.setState({ message: '' });
  };

  handleEmojiAddition = (emoji) => {
    const { message } = { ...this.state };
    this.setState({
      message: message + emoji,
    });
  };

  handleChatState = (data) => {
    this.setState({ messageList: data });
  };

  handleChange = ({ target }) => {
    this.setState({ message: target.value });
  };

  sendMessage = () => {
    if (this.state.message !== '') {
      socket.emit(
        'chat message',
        this.state.message,
        this.props.user.name,
        this.props.user._id
      );
    }
    this.setState({ message: '' });
  };

  componentDidMount = () => {
    this.setState({ username: this.props.user.name }, () => {
      socket.emit('addUser', this.state.username, this.props.user);
    });

    socket.on('addUser', (userData, userInfo) => {
      const userInformation = Object.values(userInfo);
      this.setState({ userInformation });
      const listOfUsers = Object.keys(userData);
      this.setState({ userList: listOfUsers });
    });

    DataHandle.getData(API_CONSTANTS.GET_CHATS, this.handleChatState);

    let tempChat = [];

    socket.on('chat message', (msg, uname, senderId, receiver) => {
      tempChat = [...this.state.messageList];
      tempChat.push({
        message: msg,
        sender: uname,
        receiver,
        senderId,
      });
      this.setState({ messageList: tempChat });
    });

    let privateChat = [];
    socket.on('private', (pmsg, sender, receiver, senderID, recieverID) => {
      const recieverId = this.props.history.location.pathname.split('/');
      const previousConversation = [...this.props.chats];
      if (senderID === this.props.user._id && recieverID === recieverId[2]) {
        previousConversation.push({
          message: pmsg,
          sender,
          receiver,
          senderId: senderID,
          recieverId: recieverID,
        });
        this.setState({ conversations: previousConversation });
      }
      privateChat = [...this.state.privateMsgs];
      privateChat.push({
        message: pmsg,
        sender,
        receiver,
        senderId: senderID,
        recieverId: recieverID,
      });
      this.setState({
        privateMsgs: privateChat,
      });
    });

    socket.on('addRoom', (groupData) => {
      this.setState({ roomData: groupData });
      // this.openNotification(groupData.groupname);
      toast.success(`Group ${groupData.groupname} created`);
    });

    socket.on('groupMessage', (msg, sender, groupName, groupId) => {
      const { groupMsg } = { ...this.state };
      const tempGroup = [...groupMsg];
      tempGroup.push({
        message: msg,
        sender,
        receiver: groupName,
        recieverId: groupId,
      });
      this.setState({ groupMsg: tempGroup });
    });
  };

  render() {
    return (
      <div>
        <div className="dashboard">
          <SideBar
            userList={this.state.userList}
            userInfo={this.state.userInformation}
            roomData={this.state.roomData}
          />
          <Switch>
            <Route
              exact
              path="/dashboard"
              render={() => (
                <div className="message-window">
                  <div className="dashboard-header">Dashboard</div>
                  <ChatList
                    listText={this.state.messageList}
                    currentUser={this.props.user}
                  />
                  <Form
                    handleChange={this.handleChange}
                    value={this.state.message}
                    sendMessage={this.sendMessage}
                    handleEmojiAddition={this.handleEmojiAddition}
                    handleMessagestate={this.handleMessagestate}
                  />
                </div>
              )}
            />

            <Route
              exact
              path="/dashboard/group/:room"
              render={(routeProps) => (
                <ChatRoom {...routeProps} grpMsg={this.state.groupMsg} />
              )}
            />

            <Route
              path="/dashboard/:id"
              render={(routeProps) => (
                <PrivateDashboard
                  {...routeProps}
                  listOfUsers={this.props.users}
                  privateMsg={this.state.privateMsgs}
                  conversations={this.state.conversations}
                />
              )}
            />
          </Switch>
          <ProfileSidebar user={this.props.user} />
          <ToastContainer autoClose={2000} position="bottom-right" />
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  users: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  chats: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
  user: state.auth.user,
  chats: state.chats,
});

export default withRouter(connect(mapStateToProps)(Dashboard));
