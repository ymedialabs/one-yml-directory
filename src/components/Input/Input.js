import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class Input extends Component {
  constructor() {
    super();
    this.state = {
      value: '',
    };
    this.inputRef = React.createRef();
  }

  componentDidMount() {
    const { value } = this.props;
    if (value && value !== '') {
      this.setInitialValue();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setInitialValue();
      this.inputRef.current.focus();
    }
  }

  setInitialValue = () => {
    const { value } = this.props;
    this.setState({ value });
  };

  handleChange = ({ target }) => {
    const { onChange, name } = this.props;
    this.setState(
      {
        value: target.value,
      },
      () => {
        const { value } = this.state;
        onChange(value, name);
      }
    );
  };

  getInputClass = () => cx(['input-bar', this.props.customClass]);

  resetInput = () => {
    this.setState({ value: '' });
  };

  onKeyUpHandler = (e) => {
    const { value } = this.state;
    const { handleKeyPress } = this.props;
    if (e.keyCode === 13) {
      handleKeyPress(value);
      this.resetInput();
    }
  };

  checkTapMessageRender = () =>
    this.props.showTapMessage && this.state.value.length > 0;

  render() {
    const {
      name,
      type,
      placeholder,
      showIcon,
      spanText,
      labelClass,
      tapMessage,
    } = this.props;
    const { value } = this.state;
    return (
      <form
        autoComplete="off"
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <label className={labelClass}>
          {showIcon && <span>{spanText}</span>}
          <input
            type={type}
            placeholder={placeholder}
            value={value}
            name={name}
            onChange={this.handleChange}
            className={this.getInputClass()}
            onKeyUp={this.onKeyUpHandler}
            ref={this.inputRef}
          />
        </label>
        {this.checkTapMessageRender() && (
          <span className="tap-message">{tapMessage}</span>
        )}
      </form>
    );
  }
}

Input.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  customClass: PropTypes.string,
  showIcon: PropTypes.bool,
  spanText: PropTypes.string,
  labelClass: PropTypes.string,
  handleKeyPress: PropTypes.func,
  tapMessage: PropTypes.string,
  showTapMessage: PropTypes.bool,
};

Input.defaultProps = {
  name: '',
  type: 'text',
  placeholder: '',
  value: '',
  onChange: () => {},
  customClass: 'input-field-profile',
  showIcon: false,
  spanText: '+',
  labelClass: '',
  handleKeyPress: () => {},
  tapMessage: '',
  showTapMessage: false,
};

export default Input;
