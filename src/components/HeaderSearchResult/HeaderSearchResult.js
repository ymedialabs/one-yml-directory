import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import UserIinitalComponent from '../UserIinitalComponent/UserIinitalComponent';
import { isDataExists } from '../../utils/utils';

class HeaderSearchResult extends Component {
  renderImageContainer = (user) =>
    user.avatarUrl ? (
      <img className="profile-image" src={user.avatarUrl} alt="user-profile" />
    ) : (
      <UserIinitalComponent className="profile-image" name={user.name[0]} />
    );

  render() {
    const { searchUsers } = this.props;
    return (
      <div>
        {isDataExists(searchUsers) &&
          searchUsers.map((user, index) => (
            <Link
              className="cards"
              to={`/user/${user._id}`}
              key={index}
              onClick={this.props.closeSearchResults}
            >
              <div className="header-seach-result-user-card">
                <div className="header-search-user-name">{user.name}</div>
                {this.renderImageContainer(user)}
              </div>
            </Link>
          ))}
      </div>
    );
  }
}
HeaderSearchResult.propTypes = {
  searchUsers: PropTypes.array.isRequired,
  closeSearchResults: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  searchUsers: state.searchUsers,
});
export default connect(mapStateToProps)(HeaderSearchResult);
