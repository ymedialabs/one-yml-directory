import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Input from '../Input/Input';
import PopOver from '../PopOver/PopOver';
import CheckBox from '../CheckBox/CheckBox';
import './projectsUpload.scss';
import '../AddSkills/addSkills.scss';
import { isDataExists } from '../../utils/utils';

const PROJECT_INPUT_PLACEHOLDER = 'Add projects';
const TAP_MESSAGE_PROJECT = 'Tap enter to add project';

const ProjectsUpload = ({
  customClass,
  projectsList,
  handleChange,
  handleKeyPressProject,
  projectSearchResults,
  resetSearchResults,
  handleInputChange,
  ...props
}) => {
  const [inputValue, updateValue] = useState('');
  const [showPopUp, togglePopUp] = useState(false);

  useEffect(() => {
    if (isDataExists(projectSearchResults) && inputValue.length > 2) {
      togglePopUp(true);
    }
    return () => {
      togglePopUp(false);
    };
  }, [togglePopUp, projectSearchResults, inputValue]);
  const getContainerClass = () =>
    cx(['projects-upload-container', customClass]);

  const handleListClick = (name) => {
    updateValue(name);
    resetSearchResults();
    togglePopUp(false);
  };

  const renderPopUpChildren = () =>
    projectSearchResults.map((elem) => (
      <li
        key={elem._id}
        onClick={() => {
          handleListClick(elem.name);
        }}
      >
        {elem.name}
      </li>
    ));

  const renderSearchPopUp = () => (
    <PopOver
      customClass="skills-pop-over"
      handleOutsideClick={() => {
        togglePopUp(false);
      }}
    >
      <ul>{renderPopUpChildren()}</ul>
    </PopOver>
  );

  const renderProjectsList = () => (
    <div className="project-list-section">
      {[...projectsList].map((elem, index) => (
        <CheckBox
          label={elem.name}
          inputValue={elem.isComplete}
          key={elem._id || index}
          showClose
          customClass="project-list-item"
          showCompletedText
          handleInputChange={(value) => {
            handleInputChange(value, elem);
          }}
          handleClose={(value) => {
            handleInputChange(value, elem, true);
          }}
        />
      ))}
    </div>
  );

  return (
    <div className={getContainerClass()} {...props}>
      {isDataExists(projectsList) && renderProjectsList()}
      <div className="input-container-project">
        <Input
          labelClass="add-skills-input-label"
          customClass="add-skills-input"
          placeholder={PROJECT_INPUT_PLACEHOLDER}
          showIcon
          onChange={(value) => {
            handleChange(value);
            updateValue(value);
          }}
          handleKeyPress={handleKeyPressProject}
          value={inputValue}
          showTapMessage
          tapMessage={TAP_MESSAGE_PROJECT}
        />
        {showPopUp && renderSearchPopUp()}
      </div>
    </div>
  );
};

ProjectsUpload.propTypes = {
  customClass: PropTypes.string,
  projectsList: PropTypes.array,
  handleChange: PropTypes.func,
  handleKeyPressProject: PropTypes.func,
  projectSearchResults: PropTypes.array,
  resetSearchResults: PropTypes.func,
  handleInputChange: PropTypes.func,
};

ProjectsUpload.defaultProps = {
  customClass: '',
  projectsList: [],
  handleChange: () => {},
  handleKeyPressProject: () => {},
  projectSearchResults: [],
  resetSearchResults: () => {},
  handleInputChange: () => {},
};

export default ProjectsUpload;
