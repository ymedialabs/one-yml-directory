import React from 'react';
import PropTypes from 'prop-types';

import InfiniteScroll from 'react-infinite-scroller';

/**
 * InfiniteScroll Component.
 * @extends React.component
 */
const InfiniteScrollComponent = ({
  loadItems,
  hasData,
  items,
  useWindow,
  threshold,
  initialLoad,
  ...props
}) => (
  <InfiniteScroll
    pageStart={0}
    loadMore={loadItems}
    hasMore={hasData}
    threshold={threshold}
    useWindow={useWindow}
    initialLoad={initialLoad}
    {...props}
  >
    {items}
  </InfiniteScroll>
);

InfiniteScrollComponent.propTypes = {
  items: PropTypes.any.isRequired,
  hasData: PropTypes.bool.isRequired,
  loadItems: PropTypes.func.isRequired,
  useWindow: PropTypes.bool,
  threshold: PropTypes.number,
  initialLoad: PropTypes.bool,
};

InfiniteScrollComponent.defaultProps = {
  useWindow: true,
  threshold: 80,
  initialLoad: true,
};

export default InfiniteScrollComponent;
