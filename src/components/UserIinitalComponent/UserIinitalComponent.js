import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './userInitial.scss';

const UserIinitalComponent = ({ className, name, index, ...props }) => {
  const getNameArray = () => name.split(' ').filter((elem) => elem !== '');

  const getInitialValues = () =>
    getNameArray().length > 1
      ? `${getNameArray()[0][0].toUpperCase()} ${getNameArray()[
          getNameArray().length - 1
        ][0].toUpperCase()}`
      : name[0].toUpperCase();

  const getIndexClass = () => {
    if (index < 0) {
      return '';
    }
    if (index % 2 === 0) {
      return 'even-color';
    }
    return 'odd-color';
  };

  const getContainerClass = () => cx([className, getIndexClass()]);

  return (
    <div className={getContainerClass()} {...props}>
      <p>{name ? getInitialValues() : null}</p>
    </div>
  );
};

UserIinitalComponent.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  index: PropTypes.number,
};

UserIinitalComponent.defaultProps = {
  className: '',
  name: '',
  index: -1,
};
export default UserIinitalComponent;
