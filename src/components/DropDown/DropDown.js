import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './dropDown.scss';

class DropDown extends Component {
  state = {
    value: '',
    show: false,
  };

  componentDidMount() {
    const { value } = this.props;
    if (value && value !== '') {
      this.setInitialValue();
    }
  }

  setInitialValue = () => {
    const { value } = this.props;
    this.setState({ value });
  };

  handleClick = (option) => {
    const { onChange, name } = this.props;
    this.setState(
      {
        value: option,
        show: false,
      },
      () => {
        const { value } = this.state;
        onChange(value, name);
      }
    );
  };

  showList = () => {
    this.setState((prevState) => ({
      show: !prevState.show,
    }));
  };

  renderOptionList = () =>
    this.props.data.map((option, index) => (
      <li
        className="dropdown-list-items"
        key={index}
        value={option.value}
        onClick={() => this.handleClick(option.value)}
      >
        {option.label}
      </li>
    ));

  renderIcon = (show) =>
    show ? (
      <i className="fa fa-chevron-up dropdown-arrow" />
    ) : (
      <i className="fa fa-chevron-down dropdown-arrow" />
    );

  render() {
    const { placeholder } = this.props;
    const { show, value } = this.state;
    return (
      <div className="dropdown-container">
        <div onClick={this.showList} className="dropdown-input" role="button">
          {value || placeholder}
          {this.renderIcon(show)}
        </div>
        {show && (
          <>
            <div className="arrow-up" />
            <ul className="dropdown-list">{this.renderOptionList()}</ul>
          </>
        )}
      </div>
    );
  }
}

DropDown.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  data: PropTypes.array,
};

DropDown.defaultProps = {
  value: '',
  name: '',
  placeholder: '',
  onChange: () => {},
  data: [],
};
export default DropDown;
