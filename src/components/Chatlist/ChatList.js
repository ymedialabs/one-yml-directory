import React from 'react';
import './chatlist.scss';
import PropTypes from 'prop-types';
import avatarImg from '../../assets/avatar2.jpg';

const urlRegex = require('url-regex');

const ChatList = ({ listText, currentUser }) => {
  const listOfChats = [];

  const getConversations = (messages) => {
    if (messages === undefined) {
      return;
    }

    const listItems = messages.map((message, index) => {
      let bubbleClass = 'me';
      let bubbleDirection = '';

      if (message.type === 0) {
        bubbleClass = 'you';
        bubbleDirection = 'bubble-direction-reverse';
      }
      return (
        <div className={`bubble-container ${bubbleDirection}`} key={index}>
          <img className="img-circle" src={message.image} alt="user-profile" />
          {urlRegex().test(message.text) ? (
            <div className={`bubble ${bubbleClass}`}>
              <span className="sender-name"> {`${message.sender} : `} </span>
              <a id="anchor-tag-text" href={message.text}>
                {message.text}
              </a>
            </div>
          ) : (
            <div className={`bubble ${bubbleClass}`}>
              <span className="sender-name"> {`${message.sender} : `} </span>
              {message.text}
            </div>
          )}
        </div>
      );
    });
    return listItems;
  };

  return (
    <div id="chat-item" className="chat-item">
      {listText.forEach((msgObj) => {
        if (msgObj.sender === currentUser.name) {
          listOfChats.push({
            type: 0,
            image: currentUser.avatarUrl,
            text: msgObj.message,
            sender: msgObj.sender,
          });
        } else {
          listOfChats.push({
            type: 1,
            image: avatarImg,
            text: msgObj.message,
            sender: msgObj.sender,
          });
        }
      })}
      {getConversations(listOfChats)}
    </div>
  );
};

ChatList.propTypes = {
  listText: PropTypes.array,
  currentUser: PropTypes.object.isRequired,
};

ChatList.defaultProps = {
  listText: [],
};

export default ChatList;
