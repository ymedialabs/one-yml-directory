import React from 'react';
import PropTypes from 'prop-types';
import './form.scss';
import { Icon } from 'antd';

import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';

class Form extends React.Component {
  state = {
    showEmoji: false,
  };

  handleEmojiDisplay = () => {
    this.setState((prevState) => ({ showEmoji: !prevState.showEmoji }));
  };

  handleSubmit = (event) => {
    event.preventDefault();
    document.querySelector('.msg-form').reset();
  };

  addEmoji = (e) => {
    this.props.handleEmojiAddition(e.native);
    this.handleEmojiDisplay();
  };

  render() {
    return (
      <>
        <form className="msg-form" onSubmit={this.handleSubmit}>
          <div className="text-message-input-field">
            {this.state.showEmoji ? (
              <Picker
                onSelect={this.addEmoji}
                style={{ position: 'fixed', bottom: '50px', left: '22%' }}
              />
            ) : null}
            <Icon
              type="smile"
              className="emoji-toggle-icon"
              onClick={this.handleEmojiDisplay}
            />
            <input
              className="message-field"
              type="text"
              placeholder="Type a message..."
              onChange={this.props.handleChange}
              value={this.props.value}
            />
            <button className="send-btn" onClick={this.props.sendMessage}>
              <i className="fa fa-paper-plane" />
            </button>
          </div>
        </form>
      </>
    );
  }
}

Form.propTypes = {
  handleChange: PropTypes.func,
  sendMessage: PropTypes.func,
  value: PropTypes.string,
  handleEmojiAddition: PropTypes.func,
};

Form.defaultProps = {
  handleChange: () => {},
  sendMessage: () => {},
  value: '',
  handleEmojiAddition: () => {},
};

export default Form;
