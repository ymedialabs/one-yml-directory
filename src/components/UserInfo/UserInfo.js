import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SkillsList from '../AddSkills/SkillsList';
import UserIinitalComponent from '../UserIinitalComponent/UserIinitalComponent';
import { isDataExists, showDateInFormat } from '../../utils/utils';
import FEATURES_INFO from '../../accessRestrictions';
import { checkUserAccessFeature } from '../../services/AuthService';
import './userInfo.scss';

const PLACEHOLDER_TEXTS = {
  PHONE: 'Contact Number',
  EMPLOYEE: 'YML0000',
  DOB: 'DD/MM/YYYY',
  DEPARTMENT: 'Department',
  DESIGNATION: 'Designation',
};

class UserInfo extends React.Component {
  state = {
    skillsDisplayStatus: true,
  };

  componentDidUpdate(prevProps) {
    const {
      params: { id },
    } = this.props.match;
    if (prevProps.match.params.id !== id) {
      this.checkEditRender();
    }
  }

  getIndexFromUrl = () => this.props.location.search.split('=')[1];

  renderImageContainer = () =>
    this.props.user.avatarUrl ? (
      <img
        className="profile-photo"
        src={this.props.user.avatarUrl}
        alt="user-profile"
      />
    ) : (
      <UserIinitalComponent
        className="profile-user-image"
        name={this.props.user.name}
        index={Number(this.getIndexFromUrl())}
      />
    );

  renderText = (key, placeHolderText, fromDob = false) =>
    key ? (
      <span>{fromDob ? showDateInFormat(key) : key}</span>
    ) : (
      <span className="placeholder-text">{placeHolderText}</span>
    );

  checkEditRender = () => {
    return (
      this.props.match.params.id === this.props.currentUser._id ||
      checkUserAccessFeature(FEATURES_INFO.PROFILE.EDIT_INFORMATION)
    );
  };

  getCurrentProjects = () =>
    isDataExists(this.props.user.projectInfo) &&
    [...this.props.user.projectInfo].filter((elem) => !elem.isComplete);

  getCompletedProjects = () =>
    isDataExists(this.props.user.projectInfo) &&
    [...this.props.user.projectInfo].filter((elem) => elem.isComplete);

  render() {
    const {
      user: {
        name,
        designation,
        emailId,
        phoneNumber,
        employeeId,
        skills,
        department,
        dob,
      },
    } = this.props;
    return (
      <div className="profile-container">
        <div className="user-prifile-info">
          <div className="user-details">
            <div className="profile-image-name-designation">
              {this.renderImageContainer()}
              <div className="profile-name-designation">
                <div className="profile-name">{name}</div>
                <div className="profile-designation">
                  {this.renderText(designation, PLACEHOLDER_TEXTS.DESIGNATION)}
                </div>
                {this.checkEditRender() && (
                  <Link to={`/edit-profile/${this.props.user._id}`}>
                    <p className="edit-btn">Edit profile</p>
                  </Link>
                )}
              </div>
            </div>
          </div>
          <div className="edit-profile-email-phone">
            <p className="contact-info">CONTACT INFO</p>
            <div className="profile-email">
              <i className="fa fa-envelope-o" />
              <p className="profile-emailid">{emailId}</p>
            </div>
            <div className="profile-phone">
              <i className="fa fa-phone" />
              <p className="phone-number">
                {this.renderText(phoneNumber, PLACEHOLDER_TEXTS.PHONE)}
              </p>
            </div>
          </div>
          <div className="edit-profile-id-dob-dept">
            <div>
              <label className="profile-label">EMPLOYEE ID</label>
              <div className="id-dob-dept">
                {this.renderText(employeeId, PLACEHOLDER_TEXTS.EMPLOYEE)}
              </div>
            </div>
            <div>
              <label className="profile-label">BIRTHDAY</label>
              <div className="id-dob-dept">
                {this.renderText(dob, PLACEHOLDER_TEXTS.DOB, true)}
              </div>
            </div>
            <div>
              <label className="profile-label">DEPARTMENT</label>
              <div className="id-dob-dept">
                {this.renderText(department, PLACEHOLDER_TEXTS.DEPARTMENT)}
              </div>
            </div>
          </div>
          <div>
            {isDataExists(skills) && (
              <>
                <p className="profile-skills-header">SKILLS</p>
                <SkillsList
                  items={skills}
                  skillDisplayStatus={this.state.skillsDisplayStatus}
                />
              </>
            )}
          </div>
          <div className="projects-listing-section">
            <div className="current-projects">
              {isDataExists(this.getCurrentProjects()) && (
                <>
                  <p className="profile-skills-header">CURRENT PROJECTS</p>
                  <ul>
                    {this.getCurrentProjects().map((elem) => (
                      <li key={elem._id}>{elem.name}</li>
                    ))}
                  </ul>
                </>
              )}
            </div>
            <div className="completed-projects">
              {isDataExists(this.getCompletedProjects()) && (
                <>
                  <p className="profile-skills-header">COMPLETED PROJECTS</p>
                  <ul>
                    {this.getCompletedProjects().map((elem) => (
                      <li key={elem._id}>{elem.name}</li>
                    ))}
                  </ul>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UserInfo.propTypes = {
  user: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  currentUser: state.auth.user,
});

export default withRouter(connect(mapStateToProps)(UserInfo));
