import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './popOver.scss';

const PopOver = ({
  children,
  customClass,
  handleOutsideClick,
  showArrow,
  ...props
}) => {
  const popoverRef = useRef(null);

  const handleClickOutside = ({ target }) => {
    if (popoverRef && !popoverRef.current.contains(target)) {
      handleOutsideClick();
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });

  const getContainerClass = () => cx(['popover-container', customClass]);
  return (
    <div className={getContainerClass()} ref={popoverRef} {...props}>
      {showArrow && <div className="arrow-up" />}
      {children}
    </div>
  );
};

PopOver.propTypes = {
  children: PropTypes.oneOfType([PropTypes.elementType, PropTypes.node]),
  customClass: PropTypes.string,
  handleOutsideClick: PropTypes.func,
  showArrow: PropTypes.bool,
};

PopOver.defaultProps = {
  children: <></>,
  customClass: '',
  handleOutsideClick: () => {},
  showArrow: true,
};

export default PopOver;
