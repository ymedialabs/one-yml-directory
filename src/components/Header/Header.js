import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ModalComponent from '../ModalComponent/ModalComponent';
import { logoutUser } from '../../store/actions';

import * as ROUTES from '../../constants/routes';
import UserIinitalComponent from '../UserIinitalComponent/UserIinitalComponent';
import ymlLogo from '../../assets/icn-logo3x.png';
import HeaderSearchBar from '../HeaderSearchBar/HeaderSearchBar';
import Button from '../Button/Button';
import './header.scss';

class Header extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.logout();
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  renderImageContainer = () =>
    this.props.user.avatarUrl ? (
      <img
        className="profile-image"
        src={this.props.user.avatarUrl}
        alt="user-profile"
      />
    ) : (
      <UserIinitalComponent
        className="header-profile-user-image"
        name={this.props.user.name}
      />
    );

  logout = () => {
    this.setState({ visible: false }, () => {
      this.props.logoutUser(this.props.history);
    });
  };

  getBtnConfiguration = () => [
    {
      value: 'Cancel',
      handler: this.handleCancel,
      disabled: false,
      customClass: 'logout-modal-cancel',
    },
    {
      value: 'Log out',
      handler: this.logout,
      disabled: false,
      customClass: 'logout-btn',
    },
  ];

  renderActionContainer = () =>
    this.getBtnConfiguration().map((btn, index) => (
      <Button
        key={index}
        value={btn.value}
        onClick={btn.handler}
        disabled={btn.disabled}
        customClass={btn.customClass}
      />
    ));

  renderModalChildren = () => (
    <div className="logout-modal">
      <p>Do you want to log out?</p>
      <div className="action-container">{this.renderActionContainer()}</div>
    </div>
  );

  renderModal = () => (
    <ModalComponent>{this.renderModalChildren()}</ModalComponent>
  );

  render() {
    const { visible } = this.state;
    return (
      <div className="navbar-container">
        <div className="navbar-header">
          <div className="logo-search">
            <Link to={ROUTES.HOME}>
              <img src={ymlLogo} className="yml-logo" alt="YML-logo" />
            </Link>
            <HeaderSearchBar />
          </div>
          <div className="profile-logout">
            <Link to={ROUTES.PROFILE}>
              <div className="profile-button">
                <button className="drop-down">
                  <i className="fa fa-angle-down" aria-hidden="true" />
                </button>
                {this.renderImageContainer()}
              </div>
            </Link>
            <div className="logout" onClick={this.showModal} role="button">
              <span>
                Logout <i className="fa fa-sign-out" aria-hidden="true" />
              </span>
            </div>
            {visible && this.renderModal()}
          </div>
        </div>
      </div>
    );
  }
}
Header.propTypes = {
  user: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};
const mapDispatchToProps = (dispatch) => ({
  logoutUser: (history) => dispatch(logoutUser(history)),
});
export default connect(null, mapDispatchToProps)(withRouter(Header));
