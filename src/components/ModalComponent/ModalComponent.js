import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './modalComponent.scss';

const ModalComponent = ({ customClass, children, ...props }) => {
  const updateBodyClass = (remove = false) => {
    if (remove) {
      document.querySelector('body').classList.remove('overflow-hide');
    } else if (
      !document.querySelector('body').classList.contains('overflow-hide')
    ) {
      document.querySelector('body').classList.add('overflow-hide');
    }
  };

  useEffect(() => {
    updateBodyClass();
    return () => {
      updateBodyClass(true);
    };
  });
  const getContainerClass = () => cx(['generic-modal-container', customClass]);
  return (
    <div className={getContainerClass()} {...props}>
      <div className="modal-background" />
      <div className="modal-content">{children}</div>
    </div>
  );
};

ModalComponent.propTypes = {
  customClass: PropTypes.string,
  children: PropTypes.node.isRequired,
};

ModalComponent.defaultProps = {
  customClass: '',
};

export default ModalComponent;
