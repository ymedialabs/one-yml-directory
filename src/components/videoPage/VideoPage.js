import React from 'react';
import Layout from '../Layout/Layout';
import './videoPage.scss';

const VideoPage = () => (
  <Layout>
    <div className="video-container">
      <iframe
        id="facetime"
        title="facetime service"
        width="100%"
        height="900"
        src="https://facetime.ymedia.in/"
        allow="camera;microphone"
      />
    </div>
  </Layout>
);

export default VideoPage;
