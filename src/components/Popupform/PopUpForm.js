import React, { Component } from 'react';
import './popupform.css';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import socket from '../../utils/socketConn';
import SearchBar from '../SearchBar/SearchBar';
import MemberList from '../Addmembers/MemberList';

class PopupForm extends Component {
  state = {
    groupName: '',
    groupMembers: [],
    createdBy: '',
  };

  removeItem = (itemIndex) => {
    const { groupMembers } = { ...this.state };
    const tempMembers = [...groupMembers];
    tempMembers.splice(itemIndex, 1);
    this.setState({ groupMembers: tempMembers });
  };

  handleInputChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
  };

  handleSend = () => {
    // this.props.handleChatRoomState(this.state.groupName);
    const groupId = Math.random().toString(36).substring(2, 15);
    socket.emit(
      'addRoom',
      groupId,
      this.state.groupName,
      this.state.groupMembers,
      this.state.createdBy
    );
    this.props.handleClose();
  };

  addMemberToGroup = (member) => {
    const memberList = [...this.state.groupMembers];
    if (!memberList.includes(member.name)) {
      memberList.push(member.name);
      this.setState({ groupMembers: memberList });
    }
  };

  componentDidMount = () => {
    const initialMember = [];
    initialMember.push(this.props.user.name);
    this.setState({ createdBy: this.props.user, groupMembers: initialMember });
  };

  render() {
    return (
      <div className="popup">
        <div className="popup-inner">
          <h1 className="form-header">{this.props.text}</h1>
          <form id="popupform" onClick={this.handleSubmit}>
            <input
              className="group-input-field"
              placeholder="enter group name"
              name="groupName"
              value={this.state.groupName}
              onChange={this.handleInputChange}
            />
            <SearchBar
              users={this.props.users.userList}
              onClick={this.addMemberToGroup}
              customClass="custom-search-bar"
            />
            <MemberList
              items={this.state.groupMembers}
              removeItem={this.removeItem}
            />

            <button className="submit-group" onClick={() => this.handleSend()}>
              Send
            </button>
          </form>
          <button
            className="close-btn popup-close-btn"
            onClick={() => this.props.handleClose()}
          >
            X
          </button>
        </div>
      </div>
    );
  }
}

PopupForm.propTypes = {
  text: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  user: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
  user: state.auth.user,
});

export default withRouter(connect(mapStateToProps)(PopupForm));
