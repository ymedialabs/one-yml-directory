import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import LandingPage from '../Landing/Landing';
import HomePage from '../Home/Home';
import Profile from '../Profile/Profile';
import UserDetails from '../UserDetails/UsersDetails';
import EditProfilePage from '../EditProfile/EditProfile';
import Dashboard from '../Dashboard/Dashboard';
import PageNotFound from '../PageNotFound/PageNotFound';
import VideoPage from '../videoPage/VideoPage';
import Protected from '../../Protected';
import '../../styles/_main.scss';
import { isDataExists } from '../../utils/utils';
import * as ROUTES from '../../constants/routes';
import { getSessioninfo } from '../../services/AuthService';

class HandleRoute extends Component {
  getComponentToRender = (componentName) => {
    switch (componentName) {
      case 'LandingPage':
        return LandingPage;
      case 'HomePage':
        return HomePage;
      case 'Profile':
        return Profile;
      case 'UserDetails':
        return UserDetails;
      case 'EditProfilePage':
        return EditProfilePage;
      case 'Dashboard':
        return Dashboard;
      case 'PageNotFound':
        return PageNotFound;
      case 'VideoPage':
        return VideoPage;
      default:
        return '';
    }
  };

  renderRoutes = (allowedRoutes) =>
    allowedRoutes.map((route) => (
      <Protected
        key={route.url}
        component={this.getComponentToRender(route.component)}
        path={route.url}
      />
    ));

  render() {
    const { loggedUser } = this.props;
    const userInfo = getSessioninfo() || loggedUser;
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/">
              <Redirect to={ROUTES.LANDING} />
            </Route>
            <Route exact path={ROUTES.LANDING} component={LandingPage} />
            {userInfo && isDataExists(userInfo.routesInfo) && (
              <>{this.renderRoutes(userInfo.routesInfo)}</>
            )}
            {userInfo && !isDataExists(userInfo.routesInfo) && (
              <Redirect to={ROUTES.LANDING} />
            )}
          </Switch>
        </Router>
      </div>
    );
  }
}

HandleRoute.propTypes = {
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  loggedUser: state.auth.user,
  currentUser: state.users.userData,
  projectInfo: state.users.projectInfo,
  projectSearchResults: state.users.projectSearchResults,
  employees: state.employees,
});

export default connect(mapStateToProps)(HandleRoute);
