import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  fetchAllEmployees,
  stopLoading,
  fetchMoreEmployees,
  resetListingPage,
  getAllWalkThroughPagesAction,
} from '../../store/actions';
import './home.scss';
import Layout from '../Layout/Layout';
import UserCard from '../UserCard/UserCard';
import InfiniteScroller from '../InfiniteScroller/InfiniteScroller';
import Loader from '../Loader/Loader';
import { isDataExists } from '../../utils/utils';
import { getSession } from '../../services/AuthService';

const YML_EMPLOYEE_LIST_TEXT = 'YML';
class HomePage extends React.Component {
  componentDidMount = () => {
    getSession();
    this.props.resetPage();
    this.props.fetchAllEmployees(0);
    this.props.getAllWalkThroughPages();
  };

  fetchEmployeeData = () => {
    const { page, totalPages } = this.props.employees;
    if (page <= totalPages && totalPages !== 1) {
      this.props.fetchMoreEmployees(page);
    } else {
      this.props.stopLoading();
    }
  };

  checkInfiniteScrollRender = () => {
    const { employeeList, showLoader } = this.props.employees;
    return isDataExists(employeeList) && !showLoader;
  };

  renderLoader = () => (
    <div className="loader" key={0}>
      <Loader />
    </div>
  );

  renderUserCards = () =>
    this.props.employees.employeeList.map((user, index) => (
      <UserCard user={user} key={index} index={index} />
    ));

  getHeadingText = () => (
    <span className="employee-text">
      <span className="employee-count">
        {YML_EMPLOYEE_LIST_TEXT} {`(${this.props.employees.totalCount})`}
      </span>
    </span>
  );

  renderInfiniteScroller = () => {
    const items = (
      <div className="home-body">
        <p className="home-container-heading">{this.getHeadingText()}</p>
        <div className="user-cards-container">
          {this.checkInfiniteScrollRender() && this.renderUserCards()}
        </div>
      </div>
    );
    return (
      <InfiniteScroller
        loadItems={this.fetchEmployeeData}
        hasData={this.props.employees.hasMore}
        loader={() => {
          this.renderLoader();
        }}
        items={items}
        useWindow={false}
      />
    );
  };

  render() {
    const { showLoader } = this.props.employees;
    return (
      <Layout>
        {showLoader && <Loader />}
        {this.checkInfiniteScrollRender() && this.renderInfiniteScroller()}
      </Layout>
    );
  }
}

HomePage.propTypes = {
  employees: PropTypes.object.isRequired,
  fetchAllEmployees: PropTypes.func.isRequired,
  fetchMoreEmployees: PropTypes.func.isRequired,
  stopLoading: PropTypes.func.isRequired,
  resetPage: PropTypes.func.isRequired,
  getAllWalkThroughPages: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  employees: state.employees,
  walkThrough: state.walkThrough,
  loggedUser: state.auth.user,
});
const mapDispatchToProps = (dispatch) => ({
  fetchAllEmployees: (pageNo) => dispatch(fetchAllEmployees(pageNo)),
  stopLoading: () => dispatch(stopLoading()),
  fetchMoreEmployees: (pageNo) => dispatch(fetchMoreEmployees(pageNo)),
  resetPage: () => dispatch(resetListingPage()),
  getAllWalkThroughPages: () => dispatch(getAllWalkThroughPagesAction()),
});
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
