import React, { useState } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import { showDateInFormat } from '../../utils/utils';
import 'react-datepicker/dist/react-datepicker.css';

const DatePickerComponent = ({
  customClass,
  handleChange,
  dateValue,
  yearDropdownItemNumber,
  scrollableYearDropdown,
  showYearDropdown,
  calendarClassName,
  enableBtn,
  ...props
}) => {
  const [dateVal, setDate] = useState(
    dateValue !== '' ? new Date(showDateInFormat(dateValue)) : ''
  );
  return (
    <DatePicker
      selected={dateVal}
      onChange={(date) => {
        setDate(date);
        handleChange(date);
      }}
      yearDropdownItemNumber={yearDropdownItemNumber}
      scrollableYearDropdown={scrollableYearDropdown}
      showYearDropdown={showYearDropdown}
      className={customClass}
      calendarClassName={calendarClassName}
      {...props}
    />
  );
};

DatePickerComponent.propTypes = {
  customClass: PropTypes.string,
  handleChange: PropTypes.func,
  dateValue: PropTypes.string,
  yearDropdownItemNumber: PropTypes.number,
  scrollableYearDropdown: PropTypes.bool,
  showYearDropdown: PropTypes.bool,
  calendarClassName: PropTypes.string,
  enableBtn: PropTypes.func,
};

DatePickerComponent.defaultProps = {
  customClass: '',
  handleChange: () => {},
  dateValue: '',
  yearDropdownItemNumber: 50,
  scrollableYearDropdown: true,
  showYearDropdown: true,
  calendarClassName: '',
  enableBtn: () => {},
};

export default DatePickerComponent;
