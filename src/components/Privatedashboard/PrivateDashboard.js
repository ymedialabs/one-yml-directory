/* eslint-disable camelcase */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchAllUsers, getPrivateMessages } from '../../store/actions';
import './privatedashboard.css';
import ChatList from '../Chatlist/ChatList';
import Form from '../Form/Form';
import socket from '../../utils/socketConn';
import DataHandle from '../../utils/dataHandler';
import API_CONSTANTS from '../../constants/api';

class PrivateDashboard extends Component {
  state = {
    sender: {},
    recieverId: '',
    reciever: {},
    message: '',
    privateChat: [],
    messageList: [],
  };

  handleEmojiAddition = (emoji) => {
    const { message } = { ...this.state };
    this.setState({
      message: message + emoji,
    });
  };

  handleChatState = (data) => {
    this.setState({ messageList: data });
  };

  handleChange = ({ target }) => {
    this.setState({ message: target.value });
  };

  sendIndividualMessage = () => {
    if (this.state.message !== '') {
      socket.emit(
        'private',
        this.state.message,
        this.state.sender,
        this.state.reciever
      );
    }
    this.setState({ message: '' });
  };

  componentDidMount = () => {
    const recieverId = this.props.history.location.pathname.split('/');
    this.setState({ sender: this.props.user });
    DataHandle.getPrivateMessages(
      API_CONSTANTS.GET_PRIVATE_CHATS,
      this.props.user._id,
      recieverId[2],
      this.handleChatState
    );
    this.props.getPrivateMessages(
      API_CONSTANTS.GET_PRIVATE_CHATS,
      this.props.user._id,
      recieverId[2]
    );
  };

  componentDidUpdate = (prevProps) => {
    const recieverId = prevProps.history.location.pathname.split('/');
    if (this.state.recieverId !== recieverId[2]) {
      this.updateRecieverData(recieverId);
    }
  };

  updateRecieverData = (recieverId) => {
    this.setState({ recieverId: recieverId[2] });
    const recieverObj = this.props.users.userList.filter(
      (user) => user._id === recieverId[2]
    );
    this.setState({ reciever: recieverObj[0] });
    DataHandle.getPrivateMessages(
      API_CONSTANTS.GET_PRIVATE_CHATS,
      this.props.user._id,
      recieverId[2],
      this.handleChatState
    );
    this.props.getPrivateMessages(
      API_CONSTANTS.GET_PRIVATE_CHATS,
      this.props.user._id,
      recieverId[2]
    );
  };

  UNSAFE_componentWillReceiveProps = (nextProps) => {
    const messageHistory = [...this.state.messageList];
    const recieverId = nextProps.history.location.pathname.split('/');
    const recieverObj = nextProps.users.userList.filter(
      (user) => user._id === recieverId[2]
    );
    const privateConversation = nextProps.privateMsg;
    privateConversation.forEach((elem) => {
      const msgIndex = messageHistory.findIndex(
        (eachMsg) =>
          eachMsg.senderId === elem.senderId && eachMsg.message === elem.message
      );
      if (msgIndex === -1) {
        messageHistory.push(elem);
      }
    });
    const conversationData = messageHistory.filter(
      (elem) =>
        (elem.senderId === nextProps.user._id &&
          elem.recieverId === recieverId[2]) ||
        (elem.senderId === recieverId[2] &&
          elem.recieverId === nextProps.user._id)
    );
    this.setState({ reciever: recieverObj[0], privateChat: conversationData });
  };

  render() {
    return (
      <div className="message-window">
        <div className="reciever-name">
          {this.state.reciever ? (
            <div className="header-name-profile-image">
              <div>
                <img
                  className="header-user-profile-picture"
                  src={this.state.reciever.avatarUrl}
                  alt="header-user-profile"
                />
              </div>
              <div className="header-user-profile-name">
                {this.state.reciever.name}
              </div>
            </div>
          ) : null}
        </div>
        {this.state.privateChat.length > 0 ? (
          <ChatList
            listText={this.state.privateChat}
            currentUser={this.props.user}
          />
        ) : (
          <ChatList
            listText={this.state.messageList}
            currentUser={this.props.user}
          />
        )}
        <Form
          handleChange={this.handleChange}
          value={this.state.message}
          sendMessage={this.sendIndividualMessage}
          handleEmojiAddition={this.handleEmojiAddition}
        />
      </div>
    );
  }
}

PrivateDashboard.propTypes = {
  users: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  privateMsg: PropTypes.array.isRequired,
  getPrivateMessages: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
  user: state.auth.user,
});
const mapDispatchToProps = (dispatch) => ({
  fetchAllUsers: () => dispatch(fetchAllUsers()),
  getPrivateMessages: (url, senderId, recieverId) =>
    dispatch(getPrivateMessages(url, senderId, recieverId)),
});
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PrivateDashboard)
);
