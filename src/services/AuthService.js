/* eslint-disable import/no-cycle */
import jwtDecode from 'jwt-decode';

import * as ROUTES from '../constants/routes';
import store from '../store';
import { logoutUser, setCurrentUser } from '../store/actions';
import setAuthToken from '../setAuthToken';
import { isDataExists } from '../utils/utils';
import FEATURES_INFO from '../accessRestrictions';

const redirectToLanding = () => {
  window.location.herf = ROUTES.LANDING;
};

const redirectToHome = (history) => {
  if (isDataExists(Object.keys(history))) {
    history.push(ROUTES.HOME);
  }
};

const clearHistory = () =>
  window.history.replaceState(null, null, ROUTES.LANDING);

const logout = () => {
  store.dispatch(logoutUser());
  clearHistory();
};

const getSession = (history = {}) => {
  const token = localStorage.getItem('jwtToken');
  if (token) {
    setAuthToken(token);
    const decoded = jwtDecode(token);
    store.dispatch(setCurrentUser(decoded));
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
      logout();
    } else {
      redirectToHome(history);
    }
  }
};

const setSession = (token) => {
  localStorage.setItem('jwtToken', token);
  const decoded = jwtDecode(token);
  store.dispatch(setCurrentUser(decoded));
  setAuthToken(token);
  // redirectToHome(history);
};

const getSessioninfo = () => {
  const token = localStorage.getItem('jwtToken');
  if (token) {
    setAuthToken(token);
    const decoded = jwtDecode(token);
    return decoded;
  }
};

const hasRoleAccess = (userInfo, allowedRoles) => {
  return (
    userInfo.roles.filter((result) => allowedRoles.includes(result)).length > 0
  );
};

const checkUserAccessFeature = (feature) => {
  const userInfo = getSessioninfo();
  let allowedRoles = [];
  if (userInfo) {
    switch (feature) {
      case FEATURES_INFO.PROFILE.EDIT_INFORMATION:
      case FEATURES_INFO.EDIT_PROFILE.ADMIN_ROLE:
        allowedRoles = ['superadmin'];
        break;
      default:
        break;
    }
    return hasRoleAccess(userInfo, allowedRoles);
  }
  return false;
};

export {
  redirectToHome,
  redirectToLanding,
  clearHistory,
  logout,
  getSession,
  setSession,
  getSessioninfo,
  checkUserAccessFeature,
};
