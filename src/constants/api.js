const getBaseUrl = () =>
  process.env.NODE_ENV === 'development' ? 'http://localhost:7770' : '';

const BASE_URL = getBaseUrl();

const API_CONSTANTS = {
  REGISTER_USER: `${BASE_URL}/api/register/create`,
  GET_USER_DATA: `${BASE_URL}/api/login`,
  GET_CHATS: `${BASE_URL}/api/dashboard/all`,
  GOOGLE_LOGIN_LANDING: `${BASE_URL}/`,
  GET_PRIVATE_CHATS: `${BASE_URL}/api/dashboard/private`,
  GET_GROUP_LIST: `${BASE_URL}/api/dashboard/grouplist`,
  GET_GROUP_MESSAGES: `${BASE_URL}/api/dashboard/groupmessage`,
  GET_USERLIST: `${BASE_URL}/api/dashboard/userlist`,
  GET_CURRENT_GROUP: `${BASE_URL}/api/dashboard/currentgroup`,
  USER_ROUTE: `${BASE_URL}/api/users`,
  PROJECT_ROUTE: `${BASE_URL}/api/project`,
  WALKTHROUGH_ROUTE: `${BASE_URL}/api/tutorial`,
  DEPARTMENT_ROUTE: `${BASE_URL}/api/department`,
  FEEDBACK_ROUTE: `${BASE_URL}/api/feedback`,
};

export default API_CONSTANTS;
