const ROUTE_CONSTANTS = {
  DASHBOARD_ROUTE: '/dashboard',
  HOME_ROUTE: '/home',
  VIDEO_PAGE: '/video-call',
};

export default ROUTE_CONSTANTS;
