/* eslint-disable react/jsx-curly-newline */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { getSession } from './services/AuthService';

class Protected extends Component {
  componentDidMount() {
    getSession();
  }

  render() {
    const { component: Comp, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={(props) =>
          localStorage.getItem('jwtToken') ? (
            <Comp {...props} />
          ) : (
            <Redirect to="/" />
          )
        }
      />
    );
  }
}

Protected.propTypes = {
  component: PropTypes.elementType.isRequired,
};

export default Protected;
