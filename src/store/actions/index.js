/* eslint-disable import/no-cycle */
import jwtDecode from 'jwt-decode';
import { toast } from 'react-toastify';
import {
  ADD_USER,
  FETCH_ALL_USERS,
  FETCH_USER,
  UPDATE_USER,
  SET_CURRENT_USER,
  FETCH_PRIVATE_CHATS,
  FETCH_SEARCH_USERS,
  FETCH_EMPLOYEES,
  STOP_LOAD,
  FETCH_MORE_EMPLOYEES,
  RESET_LISTING_PAGE,
  SHOW_EMPLOYEE_LOADER,
  USER_DETAILS_ACTIONS,
  WALKTHROUGH_ACTIONS,
} from './types';
import setAuthToken from '../../setAuthToken';
import * as ROUTES from '../../constants/routes';
import API_CONSTANTS from '../../constants/api';
import { setSession } from '../../services/AuthService';
import { getData, postData, putData } from '../../utils/axios';

export const createUserSuccess = (data) => ({
  type: ADD_USER,
  payload: {
    _id: data._id,
    name: data._id,
    emailId: data.emailId,
    avatarUrl: data.avatarUrl,
  },
});

export const updateUserSuccess = (data) => ({
  type: UPDATE_USER,
  payload: data,
});

const fetchUserDetailSuccess = (data) => ({
  type: USER_DETAILS_ACTIONS.GET_USER_DETAILS,
  data,
});

export const fetchUserDetail = (id) => (dispatch) => {
  const url = `${API_CONSTANTS.USER_ROUTE}/get-user-details?id=${id}`;
  return getData(url, {}, ({ data }) => {
    dispatch(fetchUserDetailSuccess(data));
  });
};

// Get All Users

export const fetchUsers = (users) => ({
  type: FETCH_ALL_USERS,
  users,
});

export const fetchAllUsers = (pageNo) => (dispatch) =>
  getData(
    API_CONSTANTS.USER_ROUTE,
    {
      params: {
        pageNo,
      },
    },
    ({ data }) => {
      dispatch(fetchUsers(data));
    }
  );

// Set current user

export const setCurrentUser = (decoded) => ({
  type: SET_CURRENT_USER,
  payload: decoded,
});

export const createUser = (user, history) => (dispatch) => {
  const url = `${API_CONSTANTS.USER_ROUTE}/add`;
  return postData(url, user, ({ data }) => {
    const { token } = { ...data };
    const decoded = jwtDecode(token);
    setSession(token, history);
    dispatch(setCurrentUser(decoded));
    dispatch(createUserSuccess(data));
  });
};

// Update User

export const updateUser = (id, user, loggedUserId) => (dispatch) => {
  try {
    const url = `${API_CONSTANTS.USER_ROUTE}/update/${id}`;
    return putData(url, { user, loggedUserId }, ({ data, status }) => {
      dispatch(updateUserSuccess(data));
      dispatch(fetchUserDetail(user._id));
      if (status === 200) {
        toast.success('UPDATED SUCCESSFULLY');
      }
    });
  } catch (e) {
    toast.warn('NOT ADDED! TRY AGAIN');
  }
};

// get current user details

export const fetchUserSuccess = (currentUser) => ({
  type: FETCH_USER,
  currentUser,
});

export const fetchUser = (id) => (dispatch) => {
  const url = `${API_CONSTANTS.USER_ROUTE}/getuser/${id}`;
  return getData(url, {}, ({ data }) => {
    dispatch(fetchUserSuccess(data));
  });
};

// fetch search user

export const fetchSearchUserSuccess = (searchUsers) => ({
  type: FETCH_SEARCH_USERS,
  searchUsers,
});

export const fetchSearchUser = (searchString) => (dispatch) => {
  const url = `${API_CONSTANTS.USER_ROUTE}/search?searchString=${searchString}`;
  return getData(
    url,
    {
      params: {
        searchString,
      },
    },
    ({ data }) => {
      dispatch(fetchSearchUserSuccess(data));
    }
  );
};

// logout action

export const logoutUser = () => (dispatch) => {
  localStorage.removeItem('jwtToken');
  setAuthToken(false);
  dispatch(setCurrentUser({}));
  window.location.assign(ROUTES.LANDING);
};

export const fetchAllEmployeesSuccess = (employees) => ({
  type: FETCH_EMPLOYEES,
  employees,
});

export const fetchAllEmployees = (pageData) => {
  const pageNo = pageData + 1;
  const url = `${API_CONSTANTS.USER_ROUTE}/employees?pageNo=${pageNo}`;
  return (dispatch) =>
    getData(
      url,
      {
        params: {
          pageNo,
        },
      },
      ({ data }) => {
        dispatch(fetchAllEmployeesSuccess(data));
      }
    );
};

export const fetchPrivateChats = (chats) => ({
  type: FETCH_PRIVATE_CHATS,
  payload: chats,
});

export const getPrivateMessages = (url, senderId, recieverId) => (dispatch) => {
  const apiUrl = `${url}?senderId=${senderId}&recieverId=${recieverId}`;
  getData(
    apiUrl,
    {
      params: {
        senderId,
        recieverId,
      },
    },
    ({ data }) => {
      dispatch(fetchPrivateChats(data));
    }
  );
};

export const stopLoading = () => ({
  type: STOP_LOAD,
});

export const fetchMoreEmployeesSuccess = (employees) => ({
  type: FETCH_MORE_EMPLOYEES,
  employees,
});

export const fetchMoreEmployees = (pageData) => {
  const pageNo = pageData + 1;
  const url = `${API_CONSTANTS.USER_ROUTE}/employees?pageNo=${pageNo}`;
  return (dispatch) =>
    getData(url, {}, ({ data }) => {
      dispatch(fetchMoreEmployeesSuccess(data));
    });
};

export const resetListingPage = () => ({
  type: RESET_LISTING_PAGE,
});

export const showHomeLoader = () => ({
  type: SHOW_EMPLOYEE_LOADER,
});

const fetchProjectResultsActionSuccess = (data) => ({
  type: USER_DETAILS_ACTIONS.GET_PROJECT_SEARCH_RESULTS,
  data,
});

export const fetchProjectResultsAction = (value) => (dispatch) => {
  const url = `${API_CONSTANTS.PROJECT_ROUTE}/project-search?searchString=${value}`;
  return getData(url, {}, ({ data }) => {
    dispatch(fetchProjectResultsActionSuccess(data));
  });
};

export const updateProjectsArrayUserAction = (project) => ({
  type: USER_DETAILS_ACTIONS.UPDATE_USER_PROJECT,
  data: project,
});

export const addProjectInfoAction = (value) => (dispatch) => {
  const url = `${API_CONSTANTS.PROJECT_ROUTE}/add`;
  return postData(
    url,
    {
      name: value,
      isComplete: false,
    },
    ({ data: { project } }) => {
      dispatch(updateProjectsArrayUserAction(project));
    }
  );
};

export const resetSearchResultsAction = () => ({
  type: USER_DETAILS_ACTIONS.RESET_SEARCH,
});

export const updateProjectsArray = (arr) => ({
  type: USER_DETAILS_ACTIONS.UPDATE_USER_PROJECT_ARRAY,
  data: arr,
});

const getAllWalkThroughPagesActionSuccess = (pages) => ({
  type: WALKTHROUGH_ACTIONS.GET_ALL_PROJECTS,
  data: pages,
});

export const getAllWalkThroughPagesAction = () => (dispatch) =>
  getData(
    `${API_CONSTANTS.WALKTHROUGH_ROUTE}/all`,
    {},
    ({ data: { pages } }) => {
      dispatch(getAllWalkThroughPagesActionSuccess(pages));
    }
  );

const getDepartmentListActionSuccess = (list) => ({
  type: USER_DETAILS_ACTIONS.GET_ALL_DEPARTMENTS,
  data: list,
});

export const getDepartmentListAction = () => (dispatch) =>
  getData(
    `${API_CONSTANTS.DEPARTMENT_ROUTE}/list`,
    {},
    ({ data: { list } }) => {
      dispatch(getDepartmentListActionSuccess(list));
    }
  );

export const addFeedBackAction = (feedback, user) => () => {
  try {
    const url = `${API_CONSTANTS.FEEDBACK_ROUTE}/add`;
    return postData(url, { user, feedback }, ({ status }) => {
      if (status === 200) {
        toast.success('UPDATED SUCCESSFULLY');
      }
    });
  } catch (e) {
    toast.warn('NOT ADDED! TRY AGAIN');
  }
};
