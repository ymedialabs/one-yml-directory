import { WALKTHROUGH_ACTIONS } from '../actions/types';

const initialState = {
  walkThroughPages: [],
};

const walkThroughReducer = (state = initialState, action) => {
  let tempState = { ...state };
  switch (action.type) {
    case WALKTHROUGH_ACTIONS.GET_ALL_PAGES:
      tempState = {
        ...tempState,
        walkThroughPages: action.response.data,
      };
      return tempState;
    default:
      return tempState;
  }
};

export default walkThroughReducer;
