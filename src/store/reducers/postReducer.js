import {
  ADD_USER,
  FETCH_ALL_USERS,
  UPDATE_USER,
  USER_DETAILS_ACTIONS,
} from '../actions/types';

const getProjectInfoArr = (projectArr, proj) => {
  const index = [...projectArr].findIndex((elem) => elem.name === proj.name);
  if (index !== -1) {
    return [...projectArr];
  }
  return [...projectArr, proj];
};

const initialState = {
  userList: [],
  showLoader: true,
  userData: {},
  projectInfo: [],
  projectSearchResults: [],
  departmentList: [],
};

export default function postReducer(state = initialState, action) {
  let tempState = { ...state };
  switch (action.type) {
    case ADD_USER:
      tempState = {
        ...tempState,
        userList: [...tempState.userList, action.payload],
      };
      return tempState;
    case FETCH_ALL_USERS:
      tempState = {
        ...tempState,
        userList: action.users,
        showLoader: false,
      };
      return tempState;
    case UPDATE_USER:
      tempState = {
        ...tempState,
        userList: [...tempState.userList, action.payload],
        showLoader: false,
      };
      return tempState;
    case USER_DETAILS_ACTIONS.GET_USER_DETAILS:
      tempState = {
        ...tempState,
        userData: { ...action.data },
        showLoader: false,
        projectInfo: [...action.data.projectInfo],
      };
      return tempState;
    case USER_DETAILS_ACTIONS.GET_PROJECT_SEARCH_RESULTS:
      tempState = {
        ...tempState,
        projectSearchResults: [...action.data],
      };
      return tempState;
    case USER_DETAILS_ACTIONS.UPDATE_USER_PROJECT:
      tempState = {
        ...tempState,
        projectSearchResults: [],
        projectInfo: getProjectInfoArr(tempState.projectInfo, action.data),
      };
      return tempState;
    case USER_DETAILS_ACTIONS.RESET_SEARCH:
      tempState = {
        ...tempState,
        projectSearchResults: [],
      };
      return tempState;
    case USER_DETAILS_ACTIONS.UPDATE_USER_PROJECT_ARRAY:
      tempState = {
        ...tempState,
        projectInfo: [...action.data],
      };
      return tempState;
    case USER_DETAILS_ACTIONS.GET_ALL_DEPARTMENTS:
      tempState = {
        ...tempState,
        departmentList: action.data,
      };
      return tempState;
    default:
      return state;
  }
}
