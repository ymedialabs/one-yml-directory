import {
  FETCH_EMPLOYEES,
  STOP_LOAD,
  FETCH_MORE_EMPLOYEES,
  RESET_LISTING_PAGE,
  SHOW_EMPLOYEE_LOADER,
} from '../actions/types';

const initialState = {
  employeeList: [],
  page: 0,
  show: 20,
  hasMore: true,
  totalPages: 0,
  showLoader: true,
  totalCount: 0,
};

export default function (state = initialState, action) {
  let tempState = { ...state };
  switch (action.type) {
    case FETCH_EMPLOYEES:
      tempState = {
        ...tempState,
        employeeList: action.employees.message,
        totalPages: action.employees.pages,
        page: 1,
        hasMore: true,
        showLoader: false,
        totalCount: action.employees.totalCount,
      };
      return tempState;
    case STOP_LOAD:
      tempState = {
        ...tempState,
        hasMore: false,
        page: 0,
      };
      return tempState;

    case FETCH_MORE_EMPLOYEES:
      tempState = {
        ...tempState,
        page: tempState.page + 1,
        employeeList: [...tempState.employeeList, ...action.employees.message],
        showLoader: false,
      };
      return tempState;
    case RESET_LISTING_PAGE:
      tempState = {
        ...tempState,
        page: 0,
      };
      return tempState;
    case SHOW_EMPLOYEE_LOADER:
      tempState = {
        ...tempState,
        showLoader: true,
      };
      return tempState;
    default:
      return state;
  }
}
