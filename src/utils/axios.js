/* eslint-disable import/no-cycle */
import axios from 'axios';
import { redirectToLanding, logout } from '../services/AuthService';

const setSessionHeader = () => {
  const sessionToken = localStorage.jwtToken;
  if (sessionToken) {
    axios.defaults.headers.common.Authorization = sessionToken;
  } else {
    // empty buffered sessionId if cookie is cleared
    delete axios.defaults.headers.common.Authorization;
  }
};

const _ajax = ({ method, url, data = {} }, cb = () => {}) => {
  setSessionHeader();
  return axios({ method, url, data })
    .then((response) => cb(response))
    .catch((error) => error.response);
};

const postData = (url, data, cb = () => {}) =>
  _ajax({ method: 'post', url, data }, cb);

const getData = (url, data, cb = () => {}) =>
  _ajax({ method: 'get', url, data }, cb);

const putData = (url, data, cb = () => {}) =>
  _ajax({ method: 'put', url, data }, cb);

const deleteData = (url, data, cb = () => {}) =>
  _ajax({ method: 'delete', url, data }, cb);

const patchData = (url, data, cb = () => {}) =>
  _ajax({ method: 'patch', url, data }, cb);

function handleAPIError(error) {
  const code = error.response.status;
  if (code === 403) {
    logout();
    redirectToLanding();
  }
  return false;
}
axios.interceptors.response.use((response) => response, handleAPIError);
export { getData, postData, putData, deleteData, patchData };
