import moment from 'moment';

const DOB_FORMAT = 'MM/DD/YYYY';
const isDataExists = (arr, numToCheck = 0) => arr && arr.length > numToCheck;
const isLoadMore = (pageNum, totalPages) =>
  pageNum < totalPages || pageNum === 0;

const showDateInFormat = (date) => moment(date).format(DOB_FORMAT);
const getEpochDate = (date) => moment().unix(date);

export { isDataExists, isLoadMore, showDateInFormat, getEpochDate };
