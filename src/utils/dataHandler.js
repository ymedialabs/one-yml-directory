/* eslint-disable func-names */
import axios from 'axios';

const DataHandle = (function () {
  const postData = (url, userObject, handleUniqueRegister) => {
    axios
      .post(url, userObject)
      .then(() => {})
      .catch(() => {
        handleUniqueRegister();
      });
  };

  const getData = (url, cb) => {
    axios
      .get(url)
      .then((res) => {
        cb(res.data);
      })
      .catch(() => {});
  };

  const authenticateUser = (url, userObject, cb) => {
    axios
      .post(url, userObject)
      .then((res) => {
        cb(res.data);
      })
      .catch(() => {});
  };

  const googgleAuthentication = (url, userObject, cb) => {
    axios
      .post(url, userObject)
      .then((res) => {
        cb(res.data);
      })
      .catch(() => {});
  };

  const getPrivateMessages = (url, senderId, recieverId, cb) => {
    axios
      .get(url, {
        params: {
          senderId,
          recieverId,
        },
      })
      .then((res) => {
        cb(res.data);
      })
      .catch(() => {});
  };

  const getgroupList = (url, cb) => {
    axios
      .get(url)
      .then((res) => {
        cb(res.data);
      })
      .catch(() => {});
  };

  const getCurrentGroup = (url, groupId, cb) => {
    axios
      .get(url, {
        params: {
          groupId,
        },
      })
      .then((res) => {
        cb(res.data);
      })
      .catch(() => {});
  };

  const getGroupMessages = (url, sender, groupId, cb) => {
    axios
      .get(url, {
        params: {
          sender,
          groupId,
        },
      })
      .then((res) => {
        cb(res.data);
      })
      .catch(() => {});
  };

  return {
    postData,
    getData,
    authenticateUser,
    googgleAuthentication,
    getPrivateMessages,
    getgroupList,
    getGroupMessages,
    getCurrentGroup,
  };
})();
export default DataHandle;
