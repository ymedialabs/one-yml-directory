const FEATURES_INFO = {
  PROFILE: {
    EDIT_INFORMATION: 'EDIT_INFORMATION',
  },
  EDIT_PROFILE: {
    ADMIN_ROLE: 'ADMIN_ROLE',
  },
};

export default FEATURES_INFO;
